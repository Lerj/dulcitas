<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('persona', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('lastname', 191);
			$table->string('phone', 191)->nullable();
			$table->string('address', 191)->nullable();
			$table->string('cedula', 20)->nullable();
			$table->date('birthday')->nullable();
			$table->integer('users_id')->unsigned()->index('users_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('persona');
	}

}
