<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name_product', 191);
			$table->text('descripcion', 65535);
			$table->integer('categories_id')->unsigned()->index('products_categories_id_foreign');
			$table->string('code', 191);
			$table->string('image', 191)->nullable();
            $table->integer('type')->nullable();
			$table->string('stock', 191);
			$table->integer('status_id')->unsigned()->index('products_status_id_foreign');
			$table->integer('sale_price');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
