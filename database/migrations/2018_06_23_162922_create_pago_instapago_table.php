<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagoInstapagoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pago_instapago', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('invoice_id')->index('pago_instapago_invoice_id_foreign');
			$table->string('referencia', 15);
			$table->text('voucher')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pago_instapago');
	}

}
