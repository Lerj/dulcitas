<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->integer('id',true);
			$table->dateTime('fecha_pago')->nullable();
			$table->string('status', 191);
			$table->integer('user_id')->unsigned()->index('usr_id');
			$table->float('total', 10, 0);
			$table->text('voucher')->nullable();
			$table->timestamps();
			$table->integer('invoice_id')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
