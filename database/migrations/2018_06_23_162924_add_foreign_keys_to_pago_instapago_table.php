<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPagoInstapagoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pago_instapago', function(Blueprint $table)
		{
			$table->foreign('invoice_id')->references('id')->on('invoice')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pago_instapago', function(Blueprint $table)
		{
			$table->dropForeign('pago_instapago_invoice_id_foreign');
		});
	}

}
