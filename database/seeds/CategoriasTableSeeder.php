<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'categoria' => 'Infantiles',
            'descripcion' => 'Productos para fiestas infantiles'
        ];
        App\Categoria::insert($data);
    }
}
