<?php
use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Persona;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
   
    	
        $rroot = Role::create([
            'id'=>1,
            'name'=>'root',
            'display_name'=>'Root',
            'description'=>'Dueño del sistema'
        ]);

        $radmin = Role::create([
            'id'=>2,
            'name'=>'admin',
            'display_name'=>'Administrador',
            'description'=>'Administrador del sistema'
        ]);

        

        $rcliente = Role::create([
            'id'=>3,
            'name'=>'cliente',
            'display_name'=>'Cliente',
            'description'=>'Cliente del sistema'
        ]);

     

       $admin = User::create([
            'user' => 'admin',
            'name' => 'admin',
            'email' => 'admin@dulcitas.com',
            'password' => bcrypt('123456'),
        ]);
        $admin->attachRole($rroot);
        $persona = Persona::create([
            "name" => 'Luis',
            'lastname' => 'Rodriguez',
            'cedula' => 24549758,
            'users_id' => $admin->id
        ]);
    
        $client = User::create([
            'user' => 'cliente',
            'name' => 'cliente',
            'email' => 'cliente@dulcitas.com',
            'password' => bcrypt('123456'),
        ]);
        $persona = Persona::create([
            "name" => 'Luis',
            'lastname' => 'Rodriguez',
            'cedula' => 24549758,
            'users_id' => $client->id
        ]);
        $client->attachRole($rcliente);

    }
}

