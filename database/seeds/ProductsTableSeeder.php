<?php

use Illuminate\Database\Seeder;
use App\Products;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        Products::create([
            'image' => 'product-2.jpg',
            'name_product' => 'Helado',
            'categories_id' => 1,
            'code' => 100000,
            'descripcion' => 'Helado',
            'stock' => 90,
            'sale_price' => 200,
            'status_id' => 1
        ]);
    }
}
