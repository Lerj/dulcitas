<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Memberships;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(ProductsTableSeeder::class);

        //Insert Roles:


        //$this->call(MembershipTableSeeder::class);
    }
}
