@extends('layouts.admin.template')


@section('contenido')
    
  
    <div class="rosw">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                
            <div class="card ">

                <div class="card-header" style=""><h3>Menú de opciones</h3></div>           
                <div class="card-body">
                        <div class="text-center">
                        <div class="row">
                            <div class="col-sm-3 col-xs-12">
                                <div class="menu-item">
                                    <a href="{{route('clientes.index')}}">
                                        <div class="menu-item-content">

                                                <span class="menu-item-icon"><span class="fa fa-users"></span></span>
                                                <h5>Clientes</h5>

                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="menu-item">
                                    <a href="{{route('admin.products.index')}}">
                                        <div class="menu-item-content">

                                            <span class="menu-item-icon"><span class="fa fa-th-list"></span></span>
                                            <h5>Productos</h5>
                                        </div>
                                    </a>


                                </div>
                            </div>
                            
                            <div class="col-sm-3">
                                <div class="menu-item">
                                    <a href="{{route('admin.payments.index')}}">
                                        <div class="menu-item-content">

                                    


                                            <span class="menu-item-icon"><span class="fa fa-dollar-sign"></span></span>
                                            <h5>Pagos</h5>

                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="menu-item">
                                    <a href="{{route('users.index')}}">
                                        <div class="menu-item-content">

                                            <span class="menu-item-icon"><span class="fa fa-user"></span></span>
                                            <h5>Usuarios</h5>

                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
     
    </div>

    </div>

    <style>




        .menu-item {
            margin: 20px;
            text-align: center;
            background-color: #6f69e4;
            color: white;
            /*! padding-bottom: 8px; */
            border-radius: 5px;
            box-shadow:1px 1px 10px #DDDDDD;
            height: 100px;
        }
        .menu-item a {
            text-decoration: none;
            color: white;
            transition: all 0.3s ease;
        }

        .menu-item-icon {

            font-size: 50px;
        }
        .menu-item h5 {s
            font-size: 15px;
            font-weight: bold;
            color: white;
            transition: all 0.3s ease;
        }
        .menu-item a:hover {
            color: #DDDDDD;
        }





    </style>
       
    
@endsection