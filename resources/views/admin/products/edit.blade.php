@extends('layouts.admin.template')

@section('contenido')

    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header"><h1>Editar Producto</h1></div>
        <div class="card-body">
            {{ Form::model($product,array('url' => ['admin/products','id'=>$product->id],'method'=>'PATCH', 'files' => true)) }}
            

            <div class="form-group">
                <label class="control-label" for="">Nombre</label>
                {{ Form::text('name_product',null,['class' => 'form-control','required' => 'required']) }}
            </div>


            <div class="form-group">
                <div class="mb-12">
                    <textarea id="descripcion" name="descripcion" style="width: 100%">
                        {{$product->descripcion}}
                     </textarea>
                </div>


                <div class="form-group">
                <label class="control-label" for="">Categoría</label>
                {{ Form::select('categories_id', $categories, null, ['placeholder' => 'Selecciona una categoría...','class' => 'custom-select']) }}
            </div>

            <div class="form-group">
                <label class="control-label" for="">Precio de Venta</label>
                {{ Form::select('status_id', $status, null, ['placeholder' => 'Selecciona un status...','class' => 'custom-select']) }}
            </div>


            <div class="form-group">
                <label class="control-label" >Código</label>
                {{ Form::text('code',null,['class' => 'form-control','required' => 'required']) }}
            </div>


            <div class="form-group">
                <label class="control-label" for="">Stock</label>
                {{ Form::number('stock',null,['class' => 'form-control','required' => 'required']) }}
            </div>

            <div class="form-group">
                <label class="control-label" for="">Precio de Venta</label>
                {{ Form::number('sale_price',null,['class' => 'form-control','required' => 'required']) }}
            </div>

            <div class="form-group">
                <label class="control-label" for="">Imagen del producto</label>
                {{Form::file('image',['class' => 'form-control'])}}
            </div>
            




            <div class="float-md-left">
                    <a href="{{route('admin.categorias.index')}}" class="btn btn-danger">Regresar</a>
                </div>

                <div class="float-md-right">
                    <button type="submit"  class="btn btn-primary">Editar</button>
                </div>

        {!! Form::close() !!}
       </div>

    </div>

    @push('scripts')
        <script>
            $(function () {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                ClassicEditor
                    .create(document.querySelector('#descripcion'))
                    .then(function (editor) {
                        // The editor instance
                    })
                    .catch(function (error) {
                        console.error(error)
                    })

            });

        </script>
        <script src="{{asset('template/plugins/ckeditor/ckeditor.js')}}"></script>
    @endpush
@endsection