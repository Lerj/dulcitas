@extends('layouts.admin.template')

@section('contenido')

    @if(count($errors)>0)
        <div class="<al></al>ert alert-danger">
            <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header"><h1>Crear producto </h1></div>
        <div class="card-body">
            {{ Form::open(array('url' => 'admin/products','method'=>'POST', 'files' => true)) }}
                    {{Form::token()}}

                <div class="form-group">
                    <label class="control-label" for="">Nombre</label>

                    <input type="text" name="name_product" class="form-control" value="{{old('name_product')}}" required placeholder="Nombre del producto">
                </div>

                <div class="form-group">
                    <div class="mb-12">
                    <textarea id="descripcion" name="descripcion" style="width: 100%">
                        Ingrese la descripción del producto...
                    </textarea>
                </div>

                </div>

                <div class="form-group">
                    <label class="control-label" for="">Categoría</label>
                    {{ Form::select('categories_id', $categories, null, ['placeholder' => 'Selecciona una categoría...','class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    <label class="control-label" for="">Status</label>
                    {{ Form::select('status_id', $status, null, ['placeholder' => 'Selecciona un status...','class' => 'form-control']) }}
                </div>


                <div class="form-group">
                    <label class="control-label" >Código</label>
                    <input type="text" name="code" placeholder="Código..." class="form-control" value="{{old('code')}}" required>
                </div>

                

                <div class="form-group">
                    <label class="control-label" for="">Stock</label>
                    <input type="number" name="stock" placeholder="Stock" class="form-control" value="{{old('stock')}}" required>
                </div>

                <div class="form-group">
                    <label class="control-label" for="">Precio de Venta</label>
                    <input type="number" name="sale_price" placeholder="Precio de venta" class="form-control" value="{{old('sale_price')}}" required>
                </div>



                <div class="form-group">
                    <label class="control-label" for="">Imagen del producto</label>
                    {{Form::file('image',['class' => 'form-control'])}}
                </div>

                









                <div class="float-md-left">
                        <a href="{{url('admin/products')}}" class="btn btn-danger">Regresar</a>
                    </div>

                    <div class="float-md-right">
                        <button type="submit"  class="btn btn-primary">Agregar</button>
                    </div>


             </form>
        </div>
    </div>


    @push('scripts')

        <script>
            $(function () {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                ClassicEditor
                  .create(document.querySelector('#descripcion'))
                  .then(function (editor) {
                    // The editor instance
                  })
                  .catch(function (error) {
                    console.error(error)
                  })

              });

        </script>
        <script src="{{asset('template/plugins/ckeditor/ckeditor.js')}}"></script>
    @endpush
@endsection