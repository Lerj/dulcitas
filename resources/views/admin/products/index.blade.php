@extends('layouts.admin.template')

@section('contenido')
    <div class="card">
        <div class="card-header"><h1>Lista de productos <a href="{{route('admin.products.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a></h1></div>
        <div class="card-body">
            <h6>Filtrar por:</h6>
            <div class="row">
                <form action="" method="GET" id="form-filter">
                    <div class="md-float-right">
                        <div class="form-group"> 
                            <label class="control-label">Categoría</label>
                            {{Form::select('category',$categories,Request::get('category'),['class' => 'form-control','id' => 'categoria-select','placeholder' => 'Selecciona...'])}}
                        </div>
                    </div>
                </form>
            </div>
            <table class="table table-striped table-condensed table-hover table-bordered table-responsive" id="data-table">
                <thead>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Status</th>
                    <th>Stock</th>
                    <th>Categoría</th>
                    <th>Precio de Venta</th>

                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name_product}}</td>
                            <td>{{$product->status->status}}</td>
                            <td>{{$product->stock}}</td>
                            <td>{{$product->category->categoria}}</td>
                            <td>{{$product->sale_price}} Bsf.</td>

                            <td>
                                <button class="btn btn-success" data-toggle="modal" data-target="#modal-show-{{$product->id}}"><i class="fa fa-eye"></i></button>

                              
                                <a href="{{route('admin.products.edit',["id" => $product->id])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>

                                <button class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-{{$product->id}}"><i class="fa fa-delete"></i></button>
                            </td>

                            @component('admin.partials.delete',['model'=>$product,'route'=>'admin.products.destroy'])
                                @slot('title') Eliminar Producto @endslot

                                @slot('content') <p>¿Estás seguro de querer eliminar este producto?</p> @endslot

                            @endcomponent
                            
                            @component('admin.partials.show',['model'=>$product])
                                @slot('title') Mostrar Producto @endslot

                                @slot('content')
                                    <h1 class="text-center">{{$product->name_product}}</h1>
                                    <img src="{{asset("images/products/$product->image")}}" alt="" class="img-responsive" --width="200px" --height="200px" />
                                        <br>
                                    <h3 class="text-center">Descripción</h3>
                                    {!! $product->descripcion !!}
                                        <br>
                                    <p><strong>Stock:</strong> {{$product->stock}}</p>
                                        <p><strong>Status: </strong> {{$product->status->status}}</p>
                                        <p><strong>Stock: </strong>{{$product->stock}}</p>
                                        <p><strong>Categoría: </strong>{{$product->category->categoria}}</p>
                                @endslot

                            @endcomponent

                        </tr>    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @push('scripts')
        <!-- DataTables -->
        <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
        <script>
            $(function () {
                $('#categoria-select').change(function() {
                    $('#form-filter').submit();
                });
                $('#data-table').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });

            });
        </script>
    @endpush

@endsection