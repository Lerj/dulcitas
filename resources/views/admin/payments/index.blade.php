@extends('layouts.admin.template')

@section('contenido')

    
    <div class="card">

        @if(Session::has('alert'))

            <div class="alert alert-{{Session::get('alert')['tipo']}}" id="alert">
                {{Session::get('alert')['mensaje']}}
            </div>
            <script>

            </script>
        @endif


        @if(count($errors->all())>0)

            <div class="alert alert-danger" id="alert">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            <script>

            </script>
        @endif
            <div class="card-header">
                <h1>Lista de pagos</h1>
            </div>

            <div class="card-body">
            <table class="table table-bordered table-condensed table-striped table-hover">
                <thead>
                    <th>ID</th>
                    <th>Usuario</th>
                    <th>N° de Factura</th>
                    <th>Total</th>
                    <th>Fecha de Solicitud</th>

                    <th>Estado</th>
                    <th>Acciones</th>

                </thead>
                <tbody>
                    @foreach($pagos as $fila)

                        <tr>
                            <td>{{$fila->id}}</td>
                            <td>{{$fila->user->user}}</td>
                            <td>{{$fila->invoice["code"]}}</td>
                            <td>{{$fila->total}}Bsf. </td>
                            <td>{{$fila->fecha_solicitud}} ({{\Carbon\Carbon::parse($fila->fecha_solicitud)->diffForHumans()}})</td>
                            <td>{{$status[$fila->status]}}</td>

                             <td>
                                 <a href="{{route('admin.payments.show',$fila->id)}}" alt="Ver" aria-labelledby="Ver" class="btn btn-success"><i class="fa fa-eye"></i></a>
                             @if($fila->status==1)

                                @endif

                                @if($fila->status==2)
                                    <button class="btn btn-primary" onclick="$('#confirmar-{{$fila->id}}').modal('show')">Confirmar</button>
                                @endif

                                @if($fila->status==3  || $fila->status==4)
                                    <button class="btn btn-primary" onclick="$('#info-{{$fila->id}}').modal('show')">Ver</button>
                                @endif
                                <button class="btn btn-danger" onclick="$('#eliminar-{{$fila->id}}').modal('show')">Eliminar</button>
                             </td>




                        </tr>

                        @include('admin.payments.modal')
                    @endforeach

                </tbody>

            </table>

            </div>
    </div>
    @push('scripts')

        <script>
            function paypal(id) {
                confirm('ATENCIÓN]\nSe redirigirá a una página para procesar el pago vía paypal\nDebe regresar a esta para confirmar la transacción');
                $("#paypal-"+id).show().find('input').prop('name','cuenta_paypal');
            }
        </script>
    @endpush
@endsection