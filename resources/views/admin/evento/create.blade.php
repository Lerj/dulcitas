@extends('layouts.admin.template')

@section('contenido')


    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif  

 
    <h1>Crear nuevo evento</h1>

    <form action="{{url('admin/eventos')}}" method="POST">
        {{ csrf_field() }}
        
        <fieldset>
            <legend>Datos del evento</legend>
            <div class="col-sm-12">
                <div class="form-group">
                        <label>Nombre del evento:</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Nombre" required value="{{old('nombre')}}">
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                        <label>Lugar:</label>
                        <input type="text" name="lugar" class="form-control" placeholder="Lugar" value="{{old('lugar')}}" required>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                        <label>Fecha:</label>
                        <input type="date" name="fecha" class="form-control" value="{{old('fecha')}}" required>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                        <label>Hora:</label>
                        <input type="time" name="hora" class="form-control" value="{{old('hora')}}" placeholder="" required>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                        <label>Categoría:</label>
                        <select name="categoria" class="form-control" required>
                            <option value="">Seleccione...</option>
                            @foreach($categorias as $categoria)
                                <option value="{{$categoria->id}}" @if(old('categoria')==$categoria->id) selected @endif>{{$categoria->categoria}}</option>
                            @endforeach
                       </select>
                </div>
            </div>

        </fieldset>


        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>

@endsection