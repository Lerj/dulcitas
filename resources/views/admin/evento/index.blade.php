@extends('layouts.admin.template')

@section('contenido')
    @if(\Session::has('info'))
    <div class="alert alert-success">
        <p>{{Session::get('info')}}</p>
    </div>
    @endif
    <h1>Lista de Eventos <a href="{{route('admin.event.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a></h1>

    <div class="col-sm-12">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <th>ID</th>
                <th>Nombre</th>
                <th>Lugar</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Categoría</th>
                <th>Acciones</th>
            </thead>

            <tbody>
                @foreach($eventos as $evento)
                    <tr>
                        <td>{{$evento->id}}</td>
                        <td>{{$evento->nombre}}</td>
                        <td>{{$evento->lugar}}</td>
                        <td>{{$evento->fecha}}</td>
                        <td>{{$evento->hora}}</td>
                        <td>{{$evento->categoria["categoria"]}}</td>
                        <td>
                            <a href="{{route('admin.ticket.index',['id'=>$evento->id])}}" class="btn btn-success"><i class="fa fa-ticket"></i> Tickets</a>
                            <a href="{{route('admin.event.edit',['id'=>$evento->id])}}" class="btn btn-primary" alt="Editar"><i class="fa fa-pencil"></i></a>
                            <button onclick="$('#modal-delete-{{$evento->id}}').modal('show')" class="btn btn-danger" alt="Borrar"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>

                    @include('admin.evento.modal')
                @endforeach
            </tbody>

        </table>

        {{$eventos->render()}}
    </div>

@endsection