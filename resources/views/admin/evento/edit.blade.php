@extends('layouts.admin.template')

@section('contenido')


    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif  

 
    <h1>Editar evento N°{{$evento->id}}</h1>

    {!! Form::open(['method'=>'PATCH','route'=>['admin.event.update',$evento->id]]) !!}

        
    {{Form::token()}}        
        <fieldset>
            <legend>Datos del evento</legend>
            <div class="col-sm-12">
                <div class="form-group">
                        <label>Nombre del evento:</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Nombre" required value="{{$evento->nombre}}">
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                        <label>Lugar:</label>
                        <input type="text" name="lugar" class="form-control" placeholder="Lugar" value="{{$evento->lugar}}" required>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                        <label>Fecha:</label>
                        <input type="date" name="fecha" class="form-control" value="{{$evento->fecha}}" required>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="form-group">
                        <label>Hora:</label>
                        <input type="time" name="hora" class="form-control" value="{{$evento->hora}}" placeholder="" required>
                </div>
            </div>
            
            <div class="col-sm-12">
                <div class="form-group">
                        <label>Categoría:</label>
                        <select name="categoria" class="form-control" required>
                            <option value="">Seleccione...</option>
                            @foreach($categorias as $categoria)
         
                                <option value="{{$categoria->id}}" @if($evento->categoria_id==$categoria->id) selected @endif>{{$categoria->categoria}}</option>
                            @endforeach
                       </select>
                </div>
            </div>

        </fieldset>

       
        <button type="submit" class="btn btn-primary">Guardar</button>

        {!! Form::close() !!}



@endsection