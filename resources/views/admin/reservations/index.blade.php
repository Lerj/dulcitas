@extends('layouts.admin.template')

@section('contenido')

    
    <div class="card">

        @if(Session::has('alert'))

            <div class="alert alert-{{Session::get('alert')['tipo']}}" id="alert">
                {{Session::get('alert')['mensaje']}}
            </div>
            <script>

            </script>
        @endif


        @if(count($errors->all())>0)

            <div class="alert alert-danger" id="alert">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            <script>

            </script>
        @endif
            <div class="card-header">
                <h1>Lista de alquileres</h1>
                <h6>Cantidad de alquileres en la lista: <strong>{{count($reservations)}}</strong></h6>
            </div>

            <form action="" method="GET" id="form-filter">
                <input type="hidden" name="filter" id="filter">
            </form>
            <div class="card-body">
                <ul class="nav nav-pills nav-fill">
                    <li class="nav-item">
                        <a class="nav-link @if (!\Request::get('filter')) active @endif" onclick="event.preventDefault(); $('#filter').val(); $('#form-filter').submit()" href="">Todos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  @if (\Request::get('filter')==1) active @endif" onclick="event.preventDefault(); $('#filter').val(1); $('#form-filter').submit()" href="">Pendientes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  @if (\Request::get('filter')==2) active @endif" onclick="event.preventDefault(); $('#filter').val(2); $('#form-filter').submit()" href="">Terminados</a>                    </li>

                </ul>
            </div>
            <table class="table table-bordered table-condensed table-striped table-hover">
                <thead>
                    <th>ID</th>
                    <th>Usuario</th>
                    <th>Fecha de la Reservación</th>
                    <th>Producto</th>
                    <th>Horas</th>
                    <th>Inicio</th>
                    <th>Fin</th>
                    <!--th>Acciones</th-->

                </thead>
                <tbody>
                    @foreach($reservations as $fila)

                        <tr>
                            <td>{{$fila->id}}</td>
                            <td>{{$fila->user->user}}</td>
                            <td>{{$fila->fecha}}</td>
                            <td>{{$fila->product->name_product}}</td>
                            <td>{{$fila->horas}}</td>
                            <td>{{$fila->start_time}}</td>
                            <td>{{$fila->end_time}}</td>
{{--                            <td>{{$fila->invoice["code"]}}</td>
                            <td>{{$fila->total}}Bsf. </td>
                            <td>{{$fila->fecha_solicitud}} ({{\Carbon\Carbon::parse($fila->fecha_solicitud)->diffForHumans()}})</td>
                            <td>{{$status[$fila->status]}}</td>

                             <td>
                                 <a href="{{route('admin.payments.show',$fila->id)}}" alt="Ver" aria-labelledby="Ver" class="btn btn-success"><i class="fa fa-eye"></i></a>
                             @if($fila->status==1)

                                @endif

                                @if($fila->status==2)
                                    <button class="btn btn-primary" onclick="$('#confirmar-{{$fila->id}}').modal('show')">Confirmar</button>
                                @endif

                                @if($fila->status==3  || $fila->status==4)
                                    <button class="btn btn-primary" onclick="$('#info-{{$fila->id}}').modal('show')">Ver</button>
                                @endif
                                <button class="btn btn-danger" onclick="$('#eliminar-{{$fila->id}}').modal('show')">Eliminar</button>
                             </td>

--}}


                        </tr>

                    @endforeach

                </tbody>

            </table>

            </div>
    </div>
    @push('scripts')


    @endpush
@endsection