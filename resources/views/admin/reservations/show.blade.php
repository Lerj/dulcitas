@extends('layouts.clientarea.template')

@section('contenido')

<div class="container">

    <div class="row">
        {{--<div class="col-md-12 mb-4">
             <ul class="list-group mb-3">

            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <div>
                    <h6 class="my-0">Second product</h6>
                    <small class="text-muted">Brief description</small>
                </div>
                <span class="text-muted">$8</span>
            </li>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
                <div>
                    <h6 class="my-0">Third item</h6>
                    <small class="text-muted">Brief description</small>
                </div>
                <span class="text-muted">$5</span>
            </li>
            <li class="list-group-item d-flex justify-content-between bg-light">
                <div class="text-success">
                    <h6 class="my-0">Promo code</h6>
                    <small>EXAMPLECODE</small>
                </div>
                <span class="text-success">-$5</span>
            </li>
            <li class="list-group-item d-flex justify-content-between">
                <span>Total (USD)</span>
                <strong>$20</strong>
            </li>
        </ul>
        </div>--}}
        <div class="col-md-12 mb-4">

                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <h1>Productos del pago <span class="badge badge-secondary badge-pill">{{count($products)}}</span></h1>
                </h4>

                @foreach($products as $product)

                    <ul class="list-group mb-3">
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>

                                <img class="img-responsive img-thumbnail" src="{{asset("images/products/".$product->options["image"])}}" alt="">
                                <h2 class="my-0">{{$product->product["name_product"]}}</h2>
                                <small class="text-muted">{{$product->quantity}} x {{$product->price}} Bs.S</small>


                                @if(isset($product->options["alquiler"]))



                                <hr>
                                <h6><strong>Dirección de alquiler: </strong> {{$product->options["address"]}}</h6>

                                <h6><strong>Horas: </strong> {{$product->options["horas"]}}</h6>

                                <h6><strong>Fecha: </strong> {{$product->options["fecha"]}}</h6>

                                <h6><strong>Hora de inicio: </strong> {{$product->options["start_time"]}}</h6>
                                <h6><strong>Fin: </strong> {{$product->options["end_time"]}}</h6>
                                @endif
                            </div>
                            <span class="text-muted">123456 Bs.S</span>
                        </li>


                    </ul>
            @endforeach

                <div class="card ">
                    <div class="card-body">
                        <div class="float-sm-left">
                            <p><strong>SubTotal:</strong> {{$payment->total}} Bs.S</p>
                            <p><strong>IVA:</strong> {{($payment->invoice["iva"]/100)*$payment->total }} Bs.S</p>
                            <p><strong>Total:</strong> {{$payment->total+(($payment->invoice["iva"]/100)*$payment->total) }} Bs.S</p>
                            <a href="{{route('admin.payments.index')}}" class="btn btn-primary">Regresar</a>


                        </div>



                    </div>

                </div>
        </div>
    </div>
</div>
    @push('scripts')

    @endpush
@endsection