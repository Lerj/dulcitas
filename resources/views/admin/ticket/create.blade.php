@extends('layouts.admin.template')

@section('contenido')
    
    <div>
        <div class="col-sm-12">
            <h3>Crear nuevo tipo de entrada del Evento N°{{$evento->id}}</h3>
            @if(count($errors->all())>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error) 
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        {!! Form::open(['method'=>'POST','url'=> ['admin/ticket']]) !!}
        {{Form::token()}}
                <input type="hidden" value="{{$evento->id}}" name="id">
                <div class="col-sm-12 col-xs-12">
                    <div class="form-group{{ $errors->has('tipo') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <label>Tipo de la entrada:</label><input type="text" value="{{old('tipo')}}" class="form-control" placeholder="Tipo" name="tipo">
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12">
                    <div class="form-group{{ $errors->has('costo') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <label>Costo:</label><input type="number" value="{{old('costo')}}" class="form-control" placeholder="Costo" name="costo">
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12">
                    <div class="form-group{{ $errors->has('disponibilidad') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <label for="user">Disponibilidad:</label><input type="number" value="{{old('disponibilidad')}}" class="form-control" placeholder="Disponibilidad" name="disponibilidad">
                        </div>
                    </div>
                </div>
    

                    
                



                <button type="submit" class="btn btn-primary">Agregar</button>
                
                <a href="{{url('admin/ticket')}}" class="btn btn-danger">Regresar</a>
            </div>

                
            
        {!! Form::close() !!}

        </div>
    </div>

    
@endsection
