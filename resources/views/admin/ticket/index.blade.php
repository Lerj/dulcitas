@extends('layouts.admin.template')

@section('contenido')   

        @if(Session::has('info'))
            <div class="alert alert-success">
                <p>{{Session::get('info')}}</p>
            </div>
        @endif

    <h1>Lista de entradas del evento N°{{$evento->id}} <a href="{{action('ticketController@create',['id'=>$evento->id])}}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a></h1>

    <table class="table table-responsive table-bordered table-condensed table-striped table-hover">
        <thead>
            <th>ID</th>
            <th>Tipo</th>
            <th>Costo</th>
            <th>Disponibilidad</th>
            <th>Acciones</th>
            
        </thead>
        <tbody>
            @foreach($entradas as $fila) 

                    <tr>
                    <td>{{$fila->id}}</td>
                    <td>{{$fila->tipo}}</td>
                    <td>{{$fila->costo}} Bsf.</td>
                    <td>{{ $fila->disponibilidad - $fila->usuarios->count()}}/{{$fila->disponibilidad}}</td>
                   
                    <td>
                        <a class="btn btn-primary" href="{{action('ticketController@edit',['id'=>$fila->id,'evento'=>$evento])}}">Editar</a>
                        <a class="btn btn-danger" onclick="$('#modal-delete-{{$fila->id}}').modal('show')">Eliminar</a>
                    </td>

                </tr>
                @include('admin.ticket.modal')
            @endforeach

        </tbody>

    </table>

    <div class="pull-left">
        <a class="btn btn-primary" href="{{route('admin.event.index')}}">Regresar</a>
    </div>

    
 

    @push('scripts')
        	<script>

            </script>
    @endpush
@endsection