@extends('layouts.admin.template')

@section('contenido')

  <h2 class="text-center">Canjear Entrada</h2>  
  <div class="col-sm-12">
    <div id="reader" style="width:300px;height:250px;margin:auto;"></div>
  </div> 

  <h3 class="text-center">Escanee el código QR de la entrada</h3>

  <h5 class="text-center">Puedes escribirlo manualmente</h5>
  <div class="col-sm-4 col-sm-offset-4">
	<div class="form-group">
	  	<div class="input-group">
		  	<input type="text" class="form-control" name="codigo" id="codigo">
			  <span class="input-group-btn"><button class="btn btn-primary" id="btn-codigo">Escanear</button></span>
		  </div>
  	</div>
  </div>

  <div id="result" class="col-sm-4 col-sm-offset-4" style="display: none">
   
  </div>

  <div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-canje">
  

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Canje de entrada</h4>
			</div>
			<div class="modal-body">
          <h3 class="text-center">¡Entrada Canjeada!</h3>
          
          <p  class="text-center" style="color: #4eb981; font-size: 150px; margin:auto; text-align: center;"><i class="fa fa-check-circle"></i></p>
          <br>
          <p><strong>Evento:</strong> <span id="evento"></span></p>
          <p><strong>Código de la entrada:</strong> <span id="cod_entrada"></span></p>
          <p><strong>Tipo de entrada:</strong> <span id="tipo_entrada"></span></p>
          
          <h4>Datos personales del cliente:</h4>
          <p><strong>Nombre y Apellido: </strong> <span id="cliente_nombre"></span></p>
          <p><strong>Cédula: </strong> <span id="cliente_cedula"></span></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				
			</div>
		</div>
	</div>
	
</div>

<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-invalido">
  

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Código invalido</h4>
                
			</div>
			<div class="modal-body">
          <h3>El código no hace referencia a ninguna entrada</h3>
         
          <p  class="text-center" style="color: #8f103b; font-size: 150px; margin:auto; text-align: center;"><i class="fa fa-exclamation-triangle "></i></p>
          <br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				
			</div>
		</div>
  </div>
  	

</div>

<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-canjeada">
  

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Entrada canjeada</h4>
                
			</div>
			<div class="modal-body">
          <h3>La entrada ya fue canjeada</h3>
         
          <p  class="text-center" style="color: #8f103b; font-size: 150px; margin:auto; text-align: center;"><i class="fa fa-exclamation-triangle "></i></p>
          <br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				
			</div>
		</div>
  </div>
  	

</div>

<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="modal-inexistente">
  

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" 
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Entrada inexistente</h4>
                
			</div>
			<div class="modal-body">
          <h3>Esta entrada no existe en nuestra base de datos</h3>
         
          <p  class="text-center" style="color: #8f103b; font-size: 150px; margin:auto; text-align: center;"><i class="fa fa-exclamation-triangle "></i></p>
          <br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				
			</div>
		</div>
  </div>
  	

</div>


@push('scripts')
    <script src="{{asset('js/jsqrcode-combined.min.js')}}"></script>
    <script src="{{asset('js/html5-qrcode.min.js')}}"></script>
    <script> var url = '{{ url('/admin/checkentrada?codigo=')}}';</script>
	<script src="{{asset('js/scan.js')}}"></script>
	<script>

		$("#btn-codigo").click(function() {
			codigoManual();
		});
		function codigoManual() {
			codigo = $("#codigo").val();
			ajaxEntrada(codigo);
		}
		$("#codigo").keydown(function(e) {
			if (e.keyCode == 13) {
				codigoManual();
			}
		});
	</script>
@endpush
@endsection

    


    

      