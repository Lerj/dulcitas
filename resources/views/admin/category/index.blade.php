@extends('layouts.admin.template')

@section('contenido')
    <div class="card">
        <div class="card-header"><h1>Lista de categorías <a href="{{route('admin.categorias.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a></h1></div>
        <div class="card-body">
            <table class="table table-striped table-condensed table-hover table-bordered">
                <thead>
                    <th>#</th>
                    <th>Categoría</th>
                    <th>Descripcion</th>
                    <th>Acciones</th>
                </thead>
                
                <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>{{$category->categoria}}</td>
                            <td>{{$category->descripcion}}</td>
                            <td>
                                <a href="{{route('admin.categorias.edit',["id" => $category->id])}}" class="btn btn-primary">Editar</a>

                                <button class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-{{$category->id}}">Eliminar</button>
                            </td>
                            @component('admin.partials.delete',['model'=>$category,'route'=>'admin.categorias.destroy'])
                                @slot('title') Eliminar Categoría @endslot

                                @slot('content') <p>¿Estás seguro de querer eliminar esta categoría?</p> @endslot

                            @endcomponent
                        </tr>    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection