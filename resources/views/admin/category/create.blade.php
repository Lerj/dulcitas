@extends('layouts.admin.template')

@section('contenido')

    @if(count($errors)>0)
        <div class="alert alert-danger">
            <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
            </ul>
        </div>
    @endif

    <div class="card">
        <div class="card-header"><h1>Crear categoría</h1></div>
        <div class="card-body">
             {{  Form::open(['route' => 'admin.categorias.store', 'method' => 'POST']) }}

                <div class="form-group">
                    <label for="">Categoria</label>
                    <input type="text" name="categoria" class="form-control" value="{{old('categoria')}}" required>
                </div>

                <div class="form-group">
                    <label for="">Descripcion</label>
                    <textarea name="descripcion"  class="form-control"  required>{{old('descripcion')}}</textarea>
                </div>



                    <div class="float-md-left">
                        <a href="{{route('admin.categorias.index')}}" class="btn btn-danger">Regresar</a>
                    </div>

                    <div class="float-md-right">
                        <button type="submit"  class="btn btn-primary">Agregar</button>
                    </div>


             {{Form::close()}}
        </div>
    </div>
@endsection