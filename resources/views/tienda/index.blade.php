@extends('layouts.tienda.template')

@section('content')

<div class="container">
        <div class="card">
           <div class="card-body">
        
              <hr>
              <div class="row">
                 <!-- col.// -->
                   <div class="col-lg-4-24 col-sm-3">
                     <div class="category-wrap dropdown py-1">
                        <button type="button" class="btn btn-light  dropdown-toggle" data-toggle="dropdown" ><i class="fa fa-bars"></i> {{ Request::query('categoria') ? $categorias[Request::query('categoria')] : 'Categorías' }}</button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{url("tienda/")}}">Ninguna</a>
                          
                           @foreach($categorias as $id => $categoria)

                              <a class="dropdown-item {{Request::query('categoria') == $id ? 'active' : ''}}" href="{{url("tienda/?categoria=$id")}}">{{$categoria}}</a>
                          
                           @endforeach

                        </div>
                     </div>
                  </div>
                  <div class="col-lg-10 col-sm-10">
                     <form action="" class="py-1">
                        <div class="input-group w-100">
                         
                           <input type="text" class="form-control" style="width:50%;" placeholder="Buscar" name="search" value="{{Request::query('search')}}">
                           <div class="input-group-append">
                              <button class="btn btn-primary" type="submit">
                              <i class="fa fa-search"></i> Buscar 
                              </button>
                           </div>
                        </div>
                     </form>
                     <!-- search-wrap .end// -->
                  </div>
                 <!-- col.// -->
              </div>
              <!-- row.// -->
           </div>
           <!-- card-body .// -->
        </div>
        <!-- card.// -->

        @if(Request::query('categoria'))
          <div class="padding-y-sm">
             <span>Categoría: {{$categorias[Request::query('categoria')]}}</span>	
          </div>
        @endif
         <div class="row-sm pt-3 mt-3">

        @foreach($products as $product)
        @php
          $iva = 16;
          $precioIva = $product->sale_price*$iva/100;
          $total = $product->sale_price+$precioIva;
        @endphp
            <div class="col-md-3 col-sm-6 ml-4">
               <figure class="card card-product">
               <div class="img-wrap"> <img src="{{asset("images/products/$product->image")}}"></div>
                  <figcaption class="info-wrap">
                  <a href="{{url("tienda/products/$product->id")}}" class="title">{{$product->name_product}}</a>
                     <div class="price-wrap">
                        <span class="price-new">{{$total}} Bs.S <br> <small class="price-old">IVA (16%): {{$precioIva}} Bs.S</small></span>
                        {{-- <del class="price-old">$1980</del> --}}
                     </div>
                     <!-- price-wrap.// -->
                  </figcaption>
               </figure>
               <!-- card // -->
            </div>

            <!-- col // -->
         @endforeach
                  </div>

        <!-- row.// -->
     </div>
@endsection