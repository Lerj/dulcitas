@extends('layouts.tienda.template')

@section('content')
	
	<section class="section-content bg padding-y border-top">
   <div class="container">
      <div class="row">
         @if(count(Cart::content())>0)
         <main class="col-sm-9">
            <div class="card">
               <table class="table table-hover shopping-cart-wrap">
                  <thead class="text-muted">
                     <tr>
                        <th scope="col">Producto</th>
                        <th scope="col" width="160">Precio Unit. (+IVA)</th>
                        <th scope="col" width="120">Cantidad</th>
                        <th scope="col" width="120">Total</th>
                     </tr>
                  </thead>
                  <tbody>
                    @foreach(Cart::content() as $product)
                      @php
                         $iva = 16;
                         $precioIva = $product->price*$iva/100;
                         $total = $product->price+$precioIva;
                       @endphp

                     <tr>
                        <td>
                           <figure class="media">
                              <div class="img-wrap"><img src="{{asset("images/products/".$product->options->product->image)}}"  class="img-thumbnail img-sm"></div>
                              <figcaption class="media-body">
                                 <h6 class="title text-truncate">{{$product->name}}</h6>
                               
                              </figcaption>
                           </figure>
                        </td>
                         <td>
                           <div class="price-wrap"> 
                              <var class="price">{{$total}} Bs.S</var> 
                           </div>
                           <!-- price-wrap .// -->
                        </td>
                        <td>
                           <select class="form-control" disabled>
                              <option>{{$product->qty}}</option>
                             
                           </select>
                        </td>
                        <td>
                           <div class="price-wrap"> 
                              <var class="price">{{$product->total}} Bs.S</var> 
                           </div>
                           <!-- price-wrap .// -->
                        </td>
                      
                     </tr>
                 
                 	@endforeach
                  </tbody>
               </table>
            </div>
            <!-- card.// -->
         </main>
         <!-- col.// -->
         <aside class="col-sm-3">
          
            <dl class="dlist-align">
               <dt>Sub-Total </dt>
               <dd class="text-right">{{Cart::subtotal()}} Bs.S</dd>
            </dl>
            <dl class="dlist-align">
               <dt>IVA:</dt>
               <dd class="text-right">{{Cart::tax()}} Bs.S</dd>
            </dl>
            <dl class="dlist-align h5">
               <dt>Total:</dt>
               <dd class="text-right"><strong>{{Cart::total()}} Bs.S</strong></dd>
            </dl>
            <hr>
            
            

            @if(\Auth::user())
            <a href="{{route('tienda.payment')}}" class="btn btn-primary btn-block">Pagar</a>
            @else
            
              <p>Debes logearte o <a href="{{route('register')}}">registrarte</a> para hacer tu pedido</p>
            @endif

            
         </aside>
         
         @else
            <h3>No tienes productos registrados, <a href="{{url('tienda')}}">añade algunos</a>.</h3>
         @endif
         <!-- col.// -->
      </div>
   </div>
   <!-- container .//  -->
</section>
@endsection