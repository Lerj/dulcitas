@extends('layouts.tienda.template')

@section('content')
	
	<div class="container">
		<div class="row justify-content-center">
			{{--!! $voucher !!--}}

			<h1>Tu pedido ha sido completado, N° {{$invoice->code}}</h1>

		</div>
		
			<div class="row">
				<a href="{{route('tienda.downloadVoucher',$invoice->id)}}" class="btn btn-primary" download><i class="fa fa-download"></i> Descargar voucher</a>
			</div>
			<div class="row">
				<a href="{{route('tienda.downloadRecibo',$invoice->id)}}" class="btn btn-primary" download><i class="fa fa-download"></i> Descargar recibo</a>
			</div>

	</div>

@endsection