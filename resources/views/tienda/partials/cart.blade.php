<div class="cd-cart-container empty">
        <a href="#" class="cd-cart-trigger">
            Carta
            <ul class="count"> <!-- cart items count -->
                <li>0</li>
                <li>0</li>
            </ul> <!-- .count -->
        </a>
    
        <div class="cd-cart">
            <div class="wrapper">
                <header>
                    <h2>Carta</h2>
                    <span class="undo">Item eliminado. <a href="#0">Deshacer</a></span>
                </header>
    
                <div class="body">
                    <ul>
                        <!-- products added to the cart will be inserted here using JavaScript -->
                    </ul>
                </div>
    
                <footer>
                    <a href="{{url('tienda/checkout')}}" class="checkout btn-checkout" ><em>Pagar - <span>0</span>Bs.S</em></a>
                </footer>
            </div>
        </div> <!-- .cd-cart -->
    </div> <!-- cd-cart-container -->
    
    
    
    
    
        @push('scripts')
            <script>
                let addProduct;
                let cantidad;
                let cartBody,cartList,cartTotal,cartTrigger,cartCount,addToCartBtn,undo,undoTimeoutId,addToCart,toggleCart,removeProduct,quickUpdateCart,updateCartCount,updateCartTotal;
                let updateCart;
                let cart;
                let products = {};
                
    
                jQuery(document).ready(function($){
                    var cartWrapper = $('.cd-cart-container');
                    //product id - you don't need a counter in your real project but you can use your real product id
                    var productId = 0;

    
                    axios.get('/tienda/products').then((e) => {
                        products = e.data;
                        
                    });
                    if( cartWrapper.length > 0 ) {
                        //store jQuery objects
                        cartBody = cartWrapper.find('.body')
                        cartList = cartBody.find('ul').eq(0);
                        cartTotal = cartWrapper.find('.checkout').find('span');
                        cartTrigger = cartWrapper.children('.cd-cart-trigger');
                        cartCount = cartTrigger.children('.count')
                        addToCartBtn = $('.cd-add-to-cart');
                        undo = cartWrapper.find('.undo');
    
                        //add product to cart
                        addToCartBtn.on('click', function(event){
                            event.preventDefault();
                            addToCart($(this));
                        });
    
                        //open/close cart
                        cartTrigger.on('click', function(event){
                            event.preventDefault();
                            toggleCart();
                        });
    
                        //close cart when clicking on the .cd-cart-container::before (bg layer)
                        cartWrapper.on('click', function(event){
                            if( $(event.target).is($(this)) ) toggleCart(true);
                        });
    
                        //delete an item from the cart
                        cartList.on('click', '.delete-item', function(event){
                            event.preventDefault();
                            removeProduct($(event.target).parents('.product'));
                        });
    
                        //update item quantity
                        cartList.on('change', 'select', function(event){
                            quickUpdateCart();
                        });
    
                        //reinsert item deleted from the cart
                        undo.on('click', 'a', function(event){
                            clearInterval(undoTimeoutId);
                            event.preventDefault();
                            cartList.find('.deleted').addClass('undo-deleted').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
                                $(this).off('webkitAnimationEnd oanimationend msAnimationEnd animationend').removeClass('deleted undo-deleted').removeAttr('style');
                                quickUpdateCart();
                            });
                            undo.removeClass('visible');
                        });
                    }
                    toggleCart = function(bool) {
                        var cartIsOpen = ( typeof bool === 'undefined' ) ? cartWrapper.hasClass('cart-open') : bool;
    
                        if( cartIsOpen ) {
                            cartWrapper.removeClass('cart-open');
                            //reset undo
                            clearInterval(undoTimeoutId);
                            undo.removeClass('visible');
                            cartList.find('.deleted').remove();
    
                            setTimeout(function(){
                                cartBody.scrollTop(0);
                                //check if cart empty to hide it
                                if( Number(cartCount.find('li').eq(0).text()) == 0) cartWrapper.addClass('empty');
                            }, 500);
                        } else {
                            cartWrapper.addClass('cart-open');
                        }
                    };
                     addToCart = function(productId,qty,orderPrice,options,call) {
                        //update cart product list
                        axios.post('{{url('tienda/cart/add')}}',{
                            id: productId,
                            qty,
                            orderPrice,
                            options
                        }).then((e) => {
                            console.log(e);
                            if (e.data.success) {
                                toastr.success("Añadido");
                                i = e.data.c;
                                addProduct(i.rowId,i.qty,i.price,i.id,i.options);
                                //call();
                                //addProduct(productId,qty,orderPrice,options);
    
                            }
    
    
                        }).catch( (e) => {
                            toastr.error("Error al guardar en el carrito");
                            console.log(e);
                        });
                        //addProduct(productId,qty,orderPrice,options);
    
                        //update number of items
    
                    };
    
                    updateCart = function() {
                        console.log('update');
                        axios.get('/tienda/cart',{})
                            .then((data) => {
                                cart = Object.values(data.data);
                                let time=250;
                                cart.forEach((i) => {
                                    setTimeout(function() {
                                        addProduct(i.rowId,i.qty,i.price,i.id,i.options);
                                    },time);
                                    time+=250;
                                });
    
                            }).catch((error) => {
    
                        });
                    };
                     addProduct = function(id,qty,orderPrice,productId,options) {
                        //this is just a product placeholder
                        //you should insert an item with the selected product info
                        //replace productId, productName, price and url with your real product info
                        //productId = productId + 1;
                        let product = products.find(x => x.id === productId);
                        var productAdded =
                            `<li class="product"    data-productId="${productId}" data-id="${id}">
                                <div class="product-image">
                                    <a href="#0">
                                        <img width="70px" height="90px" src="{{asset('images/products/')}}/${product.image}" alt="placeholder">
                                    </a>
                                </div>
                                <div class="product-details">
                                    <h3><a href="#">${product.name_product}</a></h3>
                                    <span class="price">${orderPrice} Bs.S</span>
                                    <div class="actions">
                                        <a href="#0" class="delete-item">Borrar</a>
                                        <div class="quantity">
                                            <label for="cd-product-'+ ${id} +'">Qty</label>
                                            <span class="select">
                                                <select id="cd-product-'+ id +'" name="quantity">
                                                    <option value="1">1</option>
                                                    <!--option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option-->
                                                </select>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            `;
                        cartList.prepend($(productAdded));
                        cartIsEmpty = cartWrapper.hasClass('empty');
                        updateCartCount(cartIsEmpty);
                        updateCartTotal(orderPrice, true);
                        cartWrapper.removeClass('empty');
                    };
    
                    removeProduct = function(product) {
                        if (undoTimeoutId) {
    
                            return;
                        }
                        //clearInterval(undoTimeoutId);
                        cartList.find('.deleted').remove();
    
                        let topPosition = product.offset().top - cartBody.children('ul').offset().top;
                        let productQuantity = Number(product.find('.quantity').find('select').val());
                        let tmpPrice = Number(product.find('.price').text().replace('Bs.S', ''));
                        let productTotPrice = tmpPrice * productQuantity;
    
                        product.css('top', topPosition+'px').addClass('deleted');
    
                        //update items count + total price
                        updateCartTotal(productTotPrice, false);
                        updateCartCount(true, -productQuantity);
                        undo.addClass('visible');
    
                        //wait 8sec before completely remove the item
                        undoTimeoutId = setTimeout(function(){
    
                            axios.post('/tienda/cart/del/'+rowId,{})
                                .then((data) => {
                                    if (data.data.success) {
                                        toastr.success('product eliminada');
                                        undo.removeClass('visible');
                                        cartList.find('.deleted').remove();
                                        undoTimeoutId = null;
                                    }
                                }).catch((data) => {
    
                            });
                        }, 1000);
    
                        let rowId = product[0].dataset.id;
    
    
                    };
    
                    quickUpdateCart = function() {
                        var quantity = 0;
                        var price = 0;
    
                        cartList.children('li:not(.deleted)').each(function(){
                            var singleQuantity = Number($(this).find('select').val());
                            quantity = quantity + singleQuantity;
                            price = price + singleQuantity*Number($(this).find('.price').text().replace('$', ''));
                        });
    
                        cartTotal.text(price.toFixed(2));
                        cartCount.find('li').eq(0).text(quantity);
                        cartCount.find('li').eq(1).text(quantity+1);
                    };
    
                    updateCartCount = function(emptyCart, quantity) {
                        if( typeof quantity === 'undefined' ) {
                            var actual = Number(cartCount.find('li').eq(0).text()) + 1;
                            var next = actual + 1;
    
                            if( emptyCart ) {
                                cartCount.find('li').eq(0).text(actual);
                                cartCount.find('li').eq(1).text(next);
                            } else {
                                cartCount.addClass('update-count');
    
                                setTimeout(function() {
                                    cartCount.find('li').eq(0).text(actual);
                                }, 150);
    
                                setTimeout(function() {
                                    cartCount.removeClass('update-count');
                                }, 200);
    
                                setTimeout(function() {
                                    cartCount.find('li').eq(1).text(next);
                                }, 230);
                            }
                        } else {
                            var actual = Number(cartCount.find('li').eq(0).text()) ;
                            actual = actual + quantity;
    
                            var next = actual + 1;
    
                            cartCount.find('li').eq(0).text(actual);
                            cartCount.find('li').eq(1).text(next);
                        }
                    };
    
    
                    updateCartTotal = function(price, bool) {
                        bool ? cartTotal.text( (Number(cartTotal.text()) + Number(price)).toFixed(2) )  : cartTotal.text( (Number(cartTotal.text()) - Number(price)).toFixed(2) );
                    };
                    updateCart();
    
                });
            </script>
        @endpush