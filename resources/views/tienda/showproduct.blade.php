@extends('layouts.tienda.template')


@section('content')
    <section class="section-content bg padding-y-sm">
        <div class="container">
          
           <div class="row">
              <div class="col-xl-10 col-md-9 col-sm-12">
                 <main class="card">
                    <div class="row no-gutters">
                       <aside class="col-sm-6 border-right">
                          <article class="gallery-wrap">
                             <div class="img-big-wrap">
                                <div> <a href="{{asset("images/products/$product->image")}}" data-fancybox=""><img src="{{asset("images/products/$product->image")}}"></a></div>
                             </div>
                             <!-- slider-product.// -->
                             <div class="img-small-wrap">
                              <div class="item-gallery"> <img src="{{asset("images/products/$product->image")}}"></div>
                               
                             </div>
                          </article>
                       </aside>
                       <aside class="col-sm-6">
                          <article class="card-body">
                             <!-- short-info-wrap -->
                             <h3 class="title mb-3">{{$product->name_product}}</h3>
                             <div class="mb-3"> 
                                <var class="price h3 text-warning"> 
                                <span class="currency">Bs.S </span><span class="num">{{$product->sale_price}}</span>
                                </var> 
                                
                             </div>
                             <!-- price-detail-wrap .// -->
                             <dl>
                                <dt>Description</dt>
                                <dd>
                                   <p>{!! $product->descripcion !!}
                                   </p>
                                </dd>
                             </dl>
                          
                             <!-- rating-wrap.// -->
                             <hr>
                             <div class="row">
                                <div class="col-sm-5">
                                   <dl class="dlist-inline">
                                      <dt>Cantidad: </dt>
                                      <dd>
                                         <select id="qty" class="form-control form-control-sm" style="width:70px;">
                                             @for ($i = 0; $i < 20; $i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                             @endfor  
                                          
                                             
                                         </select>
                                      </dd>
                                   </dl>
                                   <!-- item-property .// -->
                                </div>
                                <!-- col.// -->
                                
                                <!-- col.// -->
                             </div>
                             <!-- row.// -->
                             <hr>
                             <a href="javascript:void(0)" onclick="agregarProducto()" class="btn  btn-outline-warning"> Añadir al carrito </a>
                             <!-- short-info-wrap .// -->
                          </article>
                          <!-- card-body.// -->
                       </aside>
                       <!-- col.// -->
                    </div>
                    <!-- row.// -->
                 </main>
              
              </div>
         
              <!-- col // -->
           </div>
           <!-- row.// -->
        </div>
        <!-- container // -->
     </section>

    
      <script>
         function agregarProducto() {

         
            var productId = '{{$product->id}}';
            var qty = $("#qty").val();
            if (!qty || qty ==0) {
               toastr.error('Especifica una cantidad válida');
               return;
            }
            var price = {{$product->sale_price}};
            var options =
            addToCart(productId,qty,price); 
         }
      </script>
@endsection