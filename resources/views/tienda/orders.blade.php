@extends('layouts.tienda.template')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-12">
				<table class="table  table-striped">
			<thead>
				<th>Código</th>
				<th>Fecha</th>
				<th>Pagado</th>
				<th width="250">Acciones</th>
			</thead>


			<tbody>
				@foreach($orders as $o) 

					<tr>
						<td>{{$o->code}}</td>
						<td>{{$o->created_at}}</td>
						<td>{{$o->total}} Bs.S</td>
						<td>
							<button data-toggle="modal" data-target="#modal-show-{{$o->id}}" class="btn btn-primary"><i class="fa fa-eye"></i></button>
							<a download href="{{route('tienda.downloadVoucher',$o->id)}}" class="btn btn-success">Voucher</a>
							<a download href="{{route('tienda.downloadRecibo',$o->id)}}" class="btn btn-warning">Recibo</a>
						</td>
					</tr>

						@push('modals')


							<div class="modal fade modal-slide-in-right" aria-hidden="true"
						role="dialog" tabindex="-1" id="modal-show-{{$o->id}}">
							
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
								<div class="modal-header">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4 style="color:red;"><span class="glyphicon glyphicon-lock"></span> Ver orden</h4>
							        </div>
									<div class="modal-body">
										<table class="table table-striped">
											 <table class="table table-hover shopping-cart-wrap">
						                  <thead class="text-muted">
						                     <tr>
						                     	<th scope="col">Nombre</th>
						                        <th scope="col">Imagen</th>
						                        <th scope="col" width="160">Precio Unit. (+IVA)</th>
						                        <th scope="col" width="120">Cantidad</th>
						                        <th scope="col" width="120">Total</th>
						                     </tr>
						                  </thead>
						                  <tbody>
						                    @foreach($o->details as $product)
						                      @php
						                         $iva = 16;
						                         $precioIva = $product->price*$iva/100;
						                         $total = $product->price+$precioIva;
						                       @endphp

						                     <tr>
					                     	   <td>
						                         {{$product->product->name_product}}
						                           <!-- price-wrap .// -->
						                        </td>
						                        <td>
						                           <figure class="media">
						                              <div class="img-wrap"><img src="{{asset("images/products/".$product->product->image)}}"  class="img-thumbnail img-sm"></div>
						                              <figcaption class="media-body">
						                                 <h6 class="title text-truncate">{{$product->product->name}}</h6>
						                               
						                              </figcaption>
						                           </figure>
						                        </td>
						                         <td>
						                           <div class="price-wrap"> 
						                              <var class="price">{{$total}} Bs.S</var> 
						                           </div>
						                           <!-- price-wrap .// -->
						                        </td>
						                        <td>
						                           <var class="price">{{$product->quantity}}</var>
						                        </td>
						                        <td>
						                           <div class="price-wrap"> 
						                              <var class="price">{{$total * $product->quantity}} Bs.S</var> 
						                           </div>
						                           <!-- price-wrap .// -->
						                        </td>
						                      
						                     </tr>
						                 
						                 	@endforeach
						                  </tbody>
						               </table>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									</div>
								</div>
							</div>
							

						</div>

						@endpush
				@endforeach
			</tbody>
				</table>
			</div>
		</div>

	</div>




@endsection
