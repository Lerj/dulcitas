<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{route('admin.index')}}">
            {{--<img src="{{asset('admin/assets/img/brand/blue.png')}}" class="navbar-brand-img" alt="...">--}}
            Dulcitas
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">

            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="{{asset('admin/assets/img/theme/team-1-800x800.jpg')}}">
              </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">

                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
                       class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Salir</span>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a>

                </div>
            </li>
        </ul>
        <!-- Collapse -->


        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="admin/index.html">
                            <img src="admin/assets/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>

            <!-- Navigation -->
            @role('cliente')
            <ul class="navbar-nav">


                      <li class="nav-item {{set_active('clientarea')}}">
                            <a class="nav-link" href="{{route('clientarea.index')}}">
                                <i class="ni ni-tv-2 text-primary"></i> Inicio
                            </a>
                        </li>
                    
                    <li class="nav-item  {{set_active('clientarea/payments*')}}">
                        <a href="{{route('clientarea.payment.index')}}" class="nav-link {{ set_active(['admin/payments*']) }}">
                            <i class="ni ni-money-coins text-primary"></i>
                            Pagos
                        </a>
                    </li>
                   


                </ul>
            @endrole

            @role(['admin','root'])
                <ul class="navbar-nav">


                    <li class="nav-item">
                        <a class="nav-link" href="{{route('admin.index')}}">
                            <i class="ni ni-tv-2 text-primary"></i> Inicio
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.products.index')}}" class="nav-link {{ set_active(['admin/products*']) }}">
                            <i class="ni ni-basket text-primary"></i>
                            Productos
                        </a>
                    </li>

                      <li class="nav-item">
                        <a href="{{route('clientes.index')}}" class="nav-link {{ set_active(['admin/clientes*']) }}">
                            <i class="ni ni-basket text-primary"></i>
                            Clientes
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.categorias.index')}}" class="nav-link {{ set_active(['admin/categorias*']) }}">
                            <i class="ni ni-tag text-primary"></i>
                            Categorías
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{route('admin.payments.index')}}" class="nav-link {{ set_active(['admin/payments*']) }}">
                            <i class="ni ni-money-coins text-primary"></i>
                            Pagos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('users.index')}}" class="nav-link {{ set_active(['admin/users*']) }}">
                            <i class="ni ni-single-02 text-primary"></i>
                            Usuarios
                        </a>
                    </li>


                </ul>
            @endrole
{{--            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Submenu</h6>
            <!-- Navigation -->--}}

        </div>
    </div>
</nav>