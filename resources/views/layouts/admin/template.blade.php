<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
    <meta name="author" content="Creative Tim">
    <title>Dulcitas - Administración</title>
    <!-- Favicon -->
{{--
    <link href="{{asset('template/admin/assets/img/brand/favicon.png')}}" rel="icon" type="image/png">
--}}
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <!-- Icons -->
    <link href="{{asset('template/admin/assets/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
    <link href="{{asset('template/admin/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <!-- Argon CSS -->
    <link type="text/css" href="{{asset('template/admin/assets/css/argon.css?v=1.0.0')}}" rel="stylesheet">
</head>

<body>
<!-- Sidenav -->
@include('layouts.admin.partials.sidebar')
<!-- Main content -->

<div class="main-content">
    <!-- Top navbar -->
    @include('layouts.admin.partials.navbar')

    <!-- Header -->
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                <!-- Card stats -->

            </div>
        </div>
    </div>

    <!-- Page content -->
    <div class="container-fluid mt--7">

       @yield('contenido')

       @include('layouts.admin.partials.footer')
    </div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<script src="{{asset('template/admin/assets/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('template/admin/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<!-- Optional JS -->
<script src="{{asset('template/admin/assets/vendor/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{asset('template/admin/assets/vendor/chart.js/dist/Chart.extension.js')}}"></script>
<!-- Argon JS -->
<script src="{{asset('template/admin/assets/js/argon.js?v=1.0.0')}}"></script>
@stack('scripts')
</body>

</html>