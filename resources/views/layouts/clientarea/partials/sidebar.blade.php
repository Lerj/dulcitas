
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('clientarea.index')}}" class="brand-link">
        <!--img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8"-->
        <span class="brand-text font-weight-light">PromuLara</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
   
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('clientarea.index')}}" class="nav-link">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>
                            Inicio
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ set_active(['admin/pagos*'],'menu-open') }} ">
                    <a href="{{route('clientarea.payment.index')}}" class="nav-link {{ set_active(['clientarea/payments*']) }}">
                        <i class="nav-icon fa fa-dollar"></i>
                        <p>
                            Pagos
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('clientarea.payment.index')}}" class="nav-link {{ set_active(['admin/payment*']) }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Pagos</p>
                            </a>
                        </li>


                    </ul>
                </li>



                 

                


            </ul>
        </nav>
        <!-- /.clientarea-menu -->
    </div>
    <!-- /.clientarea -->
</aside>
