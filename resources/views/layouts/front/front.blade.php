
<!DOCTYPE HTML>
<html>
<head>
<title>Dulcita</title>
<link href="{{asset('frontend/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
<link href="{{asset('frontend/css/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{asset('frontend/css/toastr.min.css')}}" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="{{asset('template/dist/fonts/fonts.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('frontend/css/fwslider.css')}}" media="all">

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<script src="{{asset('frontend/js/jquery.min.js')}}"></script>
    <script src="{{asset('frontend/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('frontend/js/fwslider.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/jquery.flexisel.js')}}"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
            });
        });
     </script>
</head>
<body>
	@include('layouts.front.partials.header')
     <div class="main">
      	@yield('contenido')
	  </div>

	@include('layouts.front.partials.footer')

    <script src="{{asset('frontend/js/toastr.min.js')}}"></script>
    @stack('scripts')
</body>	
</html>