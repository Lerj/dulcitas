<div class="header">
		<div class="container">
			<div class="row">
			  <div class="col-md-12">
				 <div class="header-left">
					 <div class="logo">
						<a href="{{url('/')}}"><img src="{{asset('frontend/images/dulcita.svg')}}" style="height:50px" alt=""/></a>
					 </div>
					 <div class="menu">
						  <a class="toggleMenu" href="#"><img src="{{asset('frontend/images/nav.png')}}" alt="" /></a>
						    <ul class="nav" id="nav">

						    	<li  class="{{set_active('tienda','current')}}"><a href="{{url('tienda')}}">Tienda</a></li>

						    								
								<div class="clear"></div>
							</ul>
							<script type="text/javascript" src="{{asset('frontend/js/responsive-nav.js')}}"></script>
				    </div>							
	    		    <div class="clear"></div>
	    	    </div>
	            <div class="header_right">
	    		  <!-- start search-->

						<!----search-scripts---->
						<script src="{{asset('frontend/js/classie.js')}}"></script>
						<script src="{{asset('frontend/js/uisearch.js')}}"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
				   @include('front.partials.cart')
		        <div class="clear"></div>
	       </div>
	      </div>
		 </div>
	    </div>
	  </div>