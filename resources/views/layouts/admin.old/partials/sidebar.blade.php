
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('admin.index')}}" class="brand-link">
        <!--img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8"-->
        <span class="brand-text font-weight-light">PromuLara</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
   
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview menu-open">
                    <a href="{{route('admin.index')}}" class="nav-link">
                        <i class="nav-icon fa fa-dashboard"></i>
                        <p>
                            Inicio
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ set_active(['admin/clientes*'],'menu-open') }} ">
                    <a href="{{route('clientes.index')}}" class="nav-link {{ set_active(['admin/clientes*']) }}">
                        <i class="nav-icon fa fa-users"></i>
                        <p>
                            Clientes
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('clientes.index')}}" class="nav-link {{ set_active(['admin/clientes*']) }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Clientes</p>
                            </a>
                        </li>


                    </ul>
                </li>

                <li class="nav-item has-treeview {{ set_active(['admin/products*','admin/categorias*'],'menu-open') }} ">
                        <a href="{{route('admin.products.index')}}" class="nav-link {{ set_active(['admin/products*','admin/categorias*']) }}">
                          <i class="nav-icon fa fa-th-list"></i>
                          <p>
                            Productos
                            <i class="right fa fa-angle-left"></i>
                          </p>
                        </a>
                        
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{route('admin.products.index')}}" class="nav-link {{ set_active(['admin/products*']) }}">
                                  <i class="fa fa-circle-o nav-icon"></i>
                                  <p>Productos</p>
                                </a>
                              </li>
                              <li class="nav-item">
                                <a href="{{route('admin.categorias.index')}}" class="nav-link {{ set_active(['admin/categorias*']) }}">
                                  <i class="fa fa-circle-o nav-icon"></i>
                                  <p>Categorías</p>
                                </a>
                              </li>

                        </ul>
                      </li>
                <li class="nav-item has-treeview {{ set_active(['admin/     *'],'menu-open') }} ">
                    <a href="{{route('admin.payments.index')}}" class="nav-link {{ set_active(['admin/payments*']) }}">
                        <i class="nav-icon fa fa-dollar"></i>
                        <p>
                            Pagos
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.payments.index')}}" class="nav-link {{ set_active(['admin/payments*']) }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Pagos</p>
                            </a>
                        </li>




                    </ul>
                </li>

                <li class="nav-item has-treeview {{ set_active(['admin/users*'],'menu-open') }} ">
                    <a href="{{route('admin.products.index')}}" class="nav-link {{ set_active(['admin/users']) }}">
                        <i class="nav-icon fa fa-user"></i>
                        <p>
                            Usuarios
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('users.index')}}" class="nav-link {{ set_active(['admin/users*']) }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>




                    </ul>
                </li>

                <li class="nav-item has-treeview {{ set_active(['admin/alquiler*'],'menu-open') }} ">
                    <a href="{{route('admin.alquiler.index')}}" class="nav-link {{ set_active(['admin/alquiler']) }}">
                        <i class="nav-icon fa fa-cart"></i>
                        <p>
                            Alquiler
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('admin.alquiler.index')}}" class="nav-link {{ set_active(['admin/alquiler*']) }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Alquiler</p>
                            </a>
                        </li>




                    </ul>
                </li>


                 

                


            </ul>
        </nav>
        <!-- /.clientarea-menu -->
    </div>
    <!-- /.clientarea -->
</aside>
