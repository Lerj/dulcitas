<!DOCTYPE HTML>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="author" content="Bootstrap-ecommerce by Vosidiy">
      <title>Dulcitas</title>
      
      <!-- jQuery -->
      <script src="{{asset('template/tienda/js/jquery-2.0.0.min.js')}}" type="text/javascript"></script>
      <!-- Bootstrap4 files-->
      <script src="{{asset('template/tienda/js/bootstrap.bundle.min.js')}}" type="text/javascript"></script>
      <link href="{{asset('template/tienda/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
      <!-- Font awesome 5 -->
      <link href="{{asset('template/tienda/fonts/fontawesome/css/fontawesome-all.min.css')}}" type="text/css" rel="stylesheet">
      <!-- plugin: fancybox  -->
      <script src="{{asset('template/tienda/plugins/fancybox/fancybox.min.js')}}" type="text/javascript"></script>
      <link href="{{asset('template/tienda/plugins/fancybox/fancybox.min.css')}}" type="text/css" rel="stylesheet">
      <!-- plugin: owl carousel  -->
      <link href="{{asset('template/tienda/plugins/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
      <link href="{{asset('template/tienda/plugins/owlcarousel/assets/owl.theme.default.css')}}" rel="stylesheet">
      <script src="{{asset('template/tienda/plugins/owlcarousel/owl.carousel.min.js')}}"></script>
      <!-- custom style -->
      <link href="{{asset('template/tienda/css/ui.css')}}" rel="stylesheet" type="text/css"/>
      <link href="{{asset('template/tienda/css/cart.css')}}" rel="stylesheet" type="text/css"/>

      <link href="{{asset('template/tienda/css/responsive.css')}}" rel="stylesheet" media="only screen and (max-width: 1200px)" />
      <!-- custom javascript -->
      <script src="{{asset('template/tienda/js/script.js')}}" type="text/javascript"></script>

      <link rel="stylesheet" href="{{asset('template/tienda/css/toastr.min.css')}}">
      <script src="{{asset('template/tienda/js/toastr.min.js')}}" type="text/javascript"></script>
      

      <script type="text/javascript">
         /// some script
         
         // jquery ready start
         $(document).ready(function() {
         	// jQuery code
         
         
         }); 
         // jquery end
      </script>
   </head>
   <body>
    @stack('modals')
    <!-- ========================= HEADER SECTION ========================= -->  
    <header class="section-header">
         <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container">
               <a class="navbar-brand" href="{{url('/tienda')}}"><img class="" height="80px" width="200px" src="{{asset('img/logo.jpeg')}}" alt="Dulcitas"></a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTop" aria-controls="navbarTop" aria-expanded="false" aria-label="Toggle navigation">
               <span class="navbar-toggler-icon"></span>
               </button>
               
               <!-- collapse.// -->
            </div>
         </nav>
         <section class="header-main shadow-sm">
            <div class="container">
               <div class="row-sm align-items-center justify-content-end">
                
                  <!-- col.// -->
                  <div class="col-lg-9-24 col-sm-12">
                     <div class="widgets-wrap float-right row no-gutters py-1">
                        <div class="col-auto">
                            @guest
                            <div class="widget-header dropdown">
                                <a href="#" data-toggle="dropdown" data-offset="20,10">
                                    <div class="icontext">
                                        <div class="icon-wrap"><i class="text-primary icon-sm fa fa-user"></i></div>
                                        <div class="text-wrap text-dark">
                                        Entrar 
                                        <i class="fa fa-caret-down"></i>
                                        </div>
                                    </div>
                                </a>
                                <div class="dropdown-menu">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <form class="px-4 py-3" action="{{route('login')}}" method="POST">
                                        {{ csrf_field() }}

                                        <div class="form-group">
                                        <label>Correo electrónico</label>
                                        <input name="email" type="email" class="form-control" placeholder="email@example.com">
                                        </div>
                                        <div class="form-group">
                                        <label>Contraseña</label>
                                        <input name="password" type="password" class="form-control" placeholder="Contraseña">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Entrar</button>
                                    </form>
                                    <hr class="dropdown-divider">
                                    <a class="dropdown-item" href="{{route('register')}}">¿No tienes cuenta? Regístrate</a>
                                    <a class="dropdown-item"href="{{route('password.request')}}">Forgot password?</a>
                                </div>
                            @endguest
                            @auth
                            <div class="widget-header dropdown">
                                    <a href="#" data-toggle="dropdown" data-offset="20,10">
                                        <div class="icontext">
                                            <div class="icon-wrap"><i class="text-primary icon-sm fa fa-user"></i></div>
                                            <div class="text-wrap text-dark">
                                            Hola {{Auth::user()->name}} 
                                            <i class="fa fa-caret-down"></i>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dropdown-menu">
                                        
                                        <a href="{{route('tienda.orders')}}" class="dropdown-item">Ordenes</a>
                                        <a class="dropdown-item" href="javascript:void(0)" onclick="
                                            document.querySelector('#logout-form').submit();
                                            ">Salir
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    @csrf
                                                </form> 
                                        </a>
                                    </div>
                            
                            @endauth
                              <!--  dropdown-menu .// -->
                           </div>
                           <!-- widget-header .// -->
                        </div>
                        <!-- col.// -->
                        <div class="col-auto">
                           <a href="{{url('tienda/checkout')}}" class="widget-header">
                              <div class="icontext">
                                 <div class="icon-wrap"><i class="text-primary icon-sm fa fa-shopping-cart"></i></div>
                                 <div class="text-wrap text-dark">
                                    Carrito <br>
                                 </div>
                              </div>
                           </a>
                        </div>
                        <!-- col.// -->
                     
                        <!-- col.// -->
                     </div>
                     <!-- widgets-wrap.// row.// -->
                  </div>
                  <!-- col.// -->
               </div>
               <!-- row.// -->
            </div>
            <!-- container.// -->
         </section>
         <!-- header-main .// -->
      </header>
      <!-- section-header.// -->

      <!-- ========================= SECTION CONTENT ========================= -->
      <section class="section-content bg padding-y-sm">
         @yield('content')
         <!-- container // -->
      </section>
      <!-- ========================= SECTION CONTENT .END// ========================= -->
      <!-- ========================= FOOTER ========================= -->
      <footer class="section-footer bg-secondary">
         <div class="container">
        
         </div>
         <!-- //container -->
      </footer>

      @include('tienda.partials.cart')

      <script src="{{asset('js/axios.min.js')}}"></script>
      @stack('scripts')

      <!-- ========================= FOOTER END // ========================= -->
   </body>
</html>