<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dulcita</title>
    <link href="{{asset('/front/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/front/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('/front/css/main.css')}}"  rel="stylesheet">
    <link href="{{asset('/front/css/animate.css')}}"  rel="stylesheet">
    <link href="{{asset('/front/css/responsive.css')}}"  rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
          integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
          crossorigin=""/>

    <!--[if lt IE 9]>

    <script src="{{asset('front/js/html5shiv.js')}}"></script>
    <script src="{{asset('front/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('front/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('front/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('front/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('front/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('front/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->

<body>

<!--logos pequeños-->
<header id="header" role="banner">
    <div class="main-nav">
        <div class="container">
            <div class="header-top">
                <div class="pull-right social-icons">
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-google-plus"></i></a>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>

                        </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="scroll active"><a href="#home">Home</a></li>

                        <li class="scroll"><a href="#contact">Direccion</a></li>
                        <li class="scroll"><a href="{{url('tienda')}}">Tienda</a></li>
                        @guest
                            <li class="scroll"><a href="{{route('register')}}">Registrarse 	</a></li>
                            <li class="scroll"><a href="{{route('login')}}">Iniciar Sesion 	</a></li>

                        @endguest

                        @role('client')
                            <li class="scroll"><a href="{{route('clientarea.index')}}">Cuenta</a></li>
                        @endrole

                        @role(['admin','root'])
                        <li class="scroll"><a href="{{route('admin.index')}}">Administracion</a></li>
                        @endrole
                </div>
            </div>
        </div>
    </div>
</header>
<!--/#header-->
<section id="home">
    <div id="main-slider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#main-slider" data-slide-to="0" class="active"></li>
            <li data-target="#main-slider" data-slide-to="1"></li>
            <li data-target="#main-slider" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <img class="img-responsive" src="{{asset('front/images/slider/portada1.jpg')}}" alt="slider">

            </div>
            <div class="item">
                <img class="img-responsive" src="{{asset('front/images/slider/portada2.jpg')}}" alt="slider">
                <
            </div>
            <div class="item">
                <img class="img-responsive" src="{{asset('front/images/slider/portada4.jpg')}}" alt="slider">

            </div>
        </div>
    </div>
</section>


<!--/#home-->
<section id="contact">
    <div id="map">
        <div id="gmap-wrap">
            <div id="mapid" style="height: 300px; width: 100%"></div>

        </div>
    </div><!--/#map-->
    <div class="contact-section">

        <div class="container">

              
            <div class="row">
                <div class="col-sm-3 col-sm-offset-4">
                    <div class="contact-text">
                        <h3>Contact</h3>
                        <address>
                            E-mail: dulctiabqto@gmail.com<br>
                            Telefono:  (0251) 435.1886<br>
                        </address>
                    </div>
                    <div class="contact-address">
                        <h3>Contact</h3>
                        <address>
                            Urbanizacion fundalara,<br>
                            Avenida caroni,<br>
                            Local 490<br>
                            Barquisimeto
                        </address>
                    </div>
                </div>


                <div class="col-sm-5">

                    <div id="contact-section">
                        <h3>Enviar un mensaje</h3>
                        <div class="status alert alert-success" style="display: none"></div>
                        <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="sendemail.php">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" required="required" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="message" required="required" class="form-control" rows="4" placeholder="Ingrese su mensaje"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary pull-right">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/#contact-->

<footer id="footer">
    <div class="container">

    </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="{{asset('front/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('front/js/bootstrap.min.js')}}"></script>

<script type="text/javascript" src="{{asset('front/js/smoothscroll.js')}}"></script>
<script type="text/javascript" src="{{asset('front/js/jquery.parallax.js')}}"></script>
<script type="text/javascript" src="{{asset('front/js/coundown-timer.js')}}"></script>
<script type="text/javascript" src="{{asset('front/js/jquery.scrollTo.js')}}"></script>
<script type="text/javascript" src="{{asset('front/js/jquery.nav.js')}}"></script>
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
        integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
        crossorigin=""></script>
<script type="text/javascript" src="{{asset('front/js/main.js')}}"></script>
<script type="text/javascript">
    var mymap = L.map('mapid').setView([10.067916, -69.282608], 17);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibGVyaiIsImEiOiJjam94NHF0b3EyNGx1M3duaGhuOHg2bTFoIn0.cXKDGKGFmh15xmo-qnEC5w', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'your.mapbox.access.token'
    }).addTo(mymap);
    var marker = L.marker([10.067916, -69.282608]).addTo(mymap);
    marker.bindPopup("<b>Dulcitas</b>").openPopup();

</script>

</body>
</html>