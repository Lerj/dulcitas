@extends('layouts.front.front')

@section('contenido')

	<div class="shop_top">

		<div class="container">
			<h1>Productos en Alquiler</h1>
			<h3>Filtrar por:</h3>
			<div class="row">
				<form action="" method="GET" id="form-filter">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="search">
								<label for="">Categorías</label>
								{{Form::select('categoria',$categorias,Request::get('categoria'),['class' => 'form-control','id' => 'categoria','placeholder' => 'Seleccione...'])}}

							</label>
						</div>
					</div>


				<div class="col-sm-3">
					<div class="form-group">
						<label for="">Buscar</label>
						<div class="input-group">


							<input type="text" name="search" placeholder="Ej... Inflable" class="form-control" value="{{Request::get('search')}}">
							<div class="input-group-btn">
								<button class="btn btn-primary" type="submit">Filtrar</button>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div>
			<div class="row shop_box-top">

			@if(Request::get('search'))
				<h5>Resultados de la busqueda para <strong>{{Request::get('search')}}</strong></h5>
			@endif
			@if(count($products)>0)
				@foreach($products as $product)
					@component('front.partials.product',[
						'name' => $product->name_product,
						'precio'=> $product->sale_price,
						'link'=> "/tienda/alquiler/$product->id",
						'image' => asset("images/products/$product->image"),
						'id' => $product->id,

						'code' => $product->code,
						'alquiler' => true
						])

							@slot('content')
							{{-- {!! $product->descripcion !!} --}}
							@endslot

						@endcomponent
				@endforeach
			@else
				<p>No hay productos disponibles actualmente...</p>
			@endif
		 </div>
	   </div>


	@push('scripts')
		<script>window.csrfToken = '{{csrf_token()}}'; window.cartUrl = '{{url('tienda/cart')}}';</script>
		<script src="{{asset('template/js/cart.js')}}"></script>
		<script>
			$("#categoria").change(function(e) {
			   	e.preventDefault();
			   	$("#form-filter").submit();
			});
		</script>
	@endpush
@endsection