@php
	$iva = 16;
	$precioIva = $precio*$iva/100;
	$total = $precio+$precioIva;
@endphp
<div class="col-lg-3 col-md-6 mb-4">

	<!--Card-->
	<div class="card">

		<!--Card image-->
		<div class="view overlay">
			<img src="{{$image}}" class="card-img-top" alt=""/>

			<a href="{{$link}}">
				<div class="mask rgba-white-slight"></div>
			</a>
		</div>
		<!--Card image-->

		<!--Card content-->
		<div class="card-body text-center">
			<!--Category & Title-->
			<a href="" class="grey-text">
				<h5></h5>
			</a>
			<h5>
				<strong>
					<a href="{{$link}}" class="dark-grey-text">{{$name}}
						{{--<span class="badge badge-pill danger-color">NEW</span>--}}
					</a>
				</strong>
			</h5>

			<h4 class="font-weight-bold blue-text">
				<strong>{{$total}} Bs.S (IVA: {{$iva}} Bs.S)</strong>
			</h4>

		</div>
		<!--Card content-->

	</div>
	<!--Card-->

</div>


{{--
<div class="col-md-3 shop_box"><a href="{{$link}}">
		<img src="{{$image}}" class="img-responsive" alt=""/>
		<!--span class="new-box">
			<span class="new-label">New</span>
		</span-->
		<span class="sale-box">
			<span class="sale-label">@if($alquiler) Alquiler @else Nuevo @endif</span>
		</span>
		<div class="shop_desc">
			<h3><a href="{{$link}}">{{$name}}</a></h3>
			<small>Código: {{$code}} </small>
				{{$content}}
			<!--span class="reducedfrom"></span-->
			<br>
			<span class="actual"><strong>Precio:</strong> {{$precio}} Bs.S </span>

			<br>
			<ul class="buttons">
				<li class="cart">
					@if(!$alquiler)
						<input style="width: 40px;" id="qty_{{$code}}" type="number" value="1" max="99">
						<a href="javascript:void(0)" onclick="cartAction('add','{{$code}}',{{$id}})">Añadir al carrito</a></li>
					@endif

				<li class="shop_btn"><a href="{{$link}}">Ver más</a></li>
				<div class="clear"> </div>
		    </ul>
	    </div>
		</a>
	</div>
--}}
