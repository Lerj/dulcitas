@extends('layouts.front.front')


@section('contenido')
    <div class="shop_top">
    <div class="container">
        <div class="row">
            <div class="col-md-9 single_left">
                <div class="single_image">
                    <ul id="etalage">

                        <li>
                            <a href="">
                                <img class="etalage_thumb_image" src="{{asset('images/products/'.$product->image)}}" />
                                <img class="etalage_source_image" src="{{asset('images/products/'.$product->image)}}" />
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- end product_slider -->
                <div class="single_right">
                    <h3>{{$product->name_product}} </h3>
                    <p class="m_10">
                        {{--{!! $product->descripcion !!}--}}
                    </p>

                    <div class="btn_form">
                        <form>
                            <input type="submit" value="Comprar" title="">
                        </form>
                    </div>
                    <!--ul class="add-to-links">
                        <li><img src="images/wish.png" alt=""><a href="#">Add to wishlist</a></li>
                    </ul-->
                    <div class="social_buttons">
                        <h4>95 Items</h4>
                        <button type="button" class="btn1 btn1-default1 btn1-twitter" onclick="">
                            <i class="icon-twitter"></i> Tweet
                        </button>
                        <button type="button" class="btn1 btn1-default1 btn1-facebook" onclick="">
                            <i class="icon-facebook"></i> Compartir
                        </button>
                        <button type="button" class="btn1 btn1-default1 btn1-google" onclick="">
                            <i class="icon-google"></i> Google+
                        </button>
                        <button type="button" class="btn1 btn1-default1 btn1-pinterest" onclick="">
                            <i class="icon-pinterest"></i> Pinterest
                        </button>
                    </div>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="col-md-3">
                <div class="box-info-product">
                    <p class="price2">$130.25</p>
                    <ul class="prosuct-qty">
                        <span>Cantidad:</span>
                        <select id="qty">

                            @for($i=1;$i<=30;$i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>

                    </ul>
                    <button id="btn-add" type="submit" name="Submit" class="exclusive">
                        <span>Añadir al carrito</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="desc">
            <h4>Descripción</h4>
            <p>{!! $product->descripcion !!}</p>
        </div>
        <div class="row">
            {{-- <h4 class="m_11">Productos relacionados en la misma categoría</h4>
            <div class="col-md-4 product1">
                <img src="images/s1.jpg" class="img-responsive" alt=""/>
                <div class="shop_desc"><a href="single.html">
                    </a><h3><a href="single.html"></a><a href="#">aliquam volutp</a></h3>
                    <p>Lorem ipsum consectetuer adipiscing </p>
                    <span class="reducedfrom">$66.00</span>
                    <span class="actual">$12.00</span><br>
                    <ul class="buttons">
                        <li class="cart"><a href="#">Add To Cart</a></li>
                        <li class="shop_btn"><a href="#">Read More</a></li>
                        <div class="clear"> </div>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 product1">
                <img src="images/s2.jpg" class="img-responsive" alt=""/>
                <div class="shop_desc"><a href="single.html">
                    </a><h3><a href="single.html"></a><a href="#">aliquam volutp</a></h3>
                    <p>Lorem ipsum consectetuer adipiscing </p>
                    <span class="reducedfrom">$66.00</span>
                    <span class="actual">$12.00</span><br>
                    <ul class="buttons">
                        <li class="cart"><a href="#">Add To Cart</a></li>
                        <li class="shop_btn"><a href="#">Read More</a></li>
                        <div class="clear"> </div>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <img src="images/s3.jpg" class="img-responsive" alt=""/>
                <div class="shop_desc"><a href="single.html">
                    </a><h3><a href="single.html"></a><a href="#">aliquam volutp</a></h3>
                    <p>Lorem ipsum consectetuer adipiscing </p>
                    <span class="reducedfrom">$66.00</span>
                    <span class="actual">$12.00</span><br>
                    <ul class="buttons">
                        <li class="cart"><a href="#">Add To Cart</a></li>
                        <li class="shop_btn"><a href="#">Read More</a></li>
                        <div class="clear"> </div>
                    </ul>
                </div>
            </div>
            --}}
        </div>
    </div>
</div>
</div>

    @push('scripts')
        <!----details-product-slider--->
        <!-- Include the Etalage files -->

        <script>
            $('#btn-add').click(function() {
                id = {{$product->id}};
                cartAction('add','{{$product->code}}',{{$product->id}},$("#qty").val());

            });
        </script>
        <link rel="stylesheet" href="{{asset('frontend/css/etalage.css')}}">
        <script src="{{asset('frontend/js/jquery.etalage.min.js')}}"></script>
        <!-- Include the Etalage files -->
        <script>
            jQuery(document).ready(function($){

                $('#etalage').etalage({
                    thumb_image_width: 300,
                    thumb_image_height: 400,

                    show_hint: true,
                    click_callback: function(image_anchor, instance_id){
                        alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
                    }
                });
                // This is for the dropdown list example:
                $('.dropdownlist').change(function(){
                    etalage_show( $(this).find('option:selected').attr('class') );
                });

            });


        </script>
        <!----//details-product-slider--->
    @endpush
    @endsection