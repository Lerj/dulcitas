@extends('layouts.front.front')


@section('contenido')
    <div class="shop_top">

    <div class="container">
        <h1>Alquilar producto</h1>
        <div class="row">
            <div class="col-md-9 single_left">
                <div class="single_image">
                    <ul id="etalage">

                        <li>
                            <a href="">
                                <img class="etalage_thumb_image" src="{{asset('images/products/'.$product->image)}}" />
                                <img class="etalage_source_image" src="{{asset('images/products/'.$product->image)}}" />
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- end product_slider -->
                <div class="single_right">
                    <h3>{{$product->name_product}} </h3>
                    <p class="m_10">
                        {!! $product->descripcion !!}
                    </p>

                    {{--<div class="btn_form">
                        <form>
                            <input type="submit" value="Comprar" title="">
                        </form>
                    </div>--}}
                    <!--ul class="add-to-links">
                        <li><img src="images/wish.png" alt=""><a href="#">Add to wishlist</a></li>
                    </ul-->
                    <div class="social_buttons">
                        <br>
                        <br>
                        <button type="button" class="btn1 btn1-default1 btn1-twitter" onclick="">
                            <i class="icon-twitter"></i> Tweet
                        </button>
                        <button type="button" class="btn1 btn1-default1 btn1-facebook" onclick="">
                            <i class="icon-facebook"></i> Compartir
                        </button>
                        <button type="button" class="btn1 btn1-default1 btn1-google" onclick="">
                            <i class="icon-google"></i> Google+
                        </button>
                        <button type="button" class="btn1 btn1-default1 btn1-pinterest" onclick="">
                            <i class="icon-pinterest"></i> Pinterest
                        </button>
                    </div>
                </div>
                <div class="clear"> </div>
            </div>
            <div class="col-md-3">
                <div class="box-info-product">
                    <p class="price2">{{$product->sale_price}}  Bs.S </p>
                    <small>Precio por hora</small>
                    <br>
                    <br>
                    <ul class="prosuct-qty">

                        <span>Día</span>
                        <input type="date" id="fecha" class="form-control">

                        <br>
                        <br>
                        <span>Hora de inicio:</span>

                        <input type="time" id="start" class="form-control">
                        <br>
                        <span>Hora de fin:</span>



                        <input type="time" id="end" class="form-control">
                        <br>

                        <span>Dirección:</span>
                        <textarea id="address" class="form-control"></textarea>
                        <br>
                        <br>
                        <span>Información del alquiler</span>
                        <span>Horas: <i id="horas"></i></span>

                        <span>Total: <i id="total"></i> Bs.S</span>

                        <br>
                        <br>
                        <div class="alert alert-danger" id="msg" style="display:none;">
                            <i class="fa fa-warning"></i><p></p>
                        </div>
                    </ul>
                    <button id="btn-add" type="submit" name="Submit" class="exclusive">
                        <span>Añadir al carrito </span>
                    </button>
                    <div class="lds-ring" id="loader" style="display:none;"><div></div><div></div><div></div><div></div>
                </div>
            </div>
        </div>

        <div class="row">
            {{-- <h4 class="m_11">Productos relacionados en la misma categoría</h4>
            <div class="col-md-4 product1">
                <img src="images/s1.jpg" class="img-responsive" alt=""/>
                <div class="shop_desc"><a href="single.html">
                    </a><h3><a href="single.html"></a><a href="#">aliquam volutp</a></h3>
                    <p>Lorem ipsum consectetuer adipiscing </p>
                    <span class="reducedfrom">$66.00</span>
                    <span class="actual">$12.00</span><br>
                    <ul class="buttons">
                        <li class="cart"><a href="#">Add To Cart</a></li>
                        <li class="shop_btn"><a href="#">Read More</a></li>
                        <div class="clear"> </div>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 product1">
                <img src="images/s2.jpg" class="img-responsive" alt=""/>
                <div class="shop_desc"><a href="single.html">
                    </a><h3><a href="single.html"></a><a href="#">aliquam volutp</a></h3>
                    <p>Lorem ipsum consectetuer adipiscing </p>
                    <span class="reducedfrom">$66.00</span>
                    <span class="actual">$12.00</span><br>
                    <ul class="buttons">
                        <li class="cart"><a href="#">Add To Cart</a></li>
                        <li class="shop_btn"><a href="#">Read More</a></li>
                        <div class="clear"> </div>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <img src="images/s3.jpg" class="img-responsive" alt=""/>
                <div class="shop_desc"><a href="single.html">
                    </a><h3><a href="single.html"></a><a href="#">aliquam volutp</a></h3>
                    <p>Lorem ipsum consectetuer adipiscing </p>
                    <span class="reducedfrom">$66.00</span>
                    <span class="actual">$12.00</span><br>
                    <ul class="buttons">
                        <li class="cart"><a href="#">Add To Cart</a></li>
                        <li class="shop_btn"><a href="#">Read More</a></li>
                        <div class="clear"> </div>
                    </ul>
                </div>
            </div>
            --}}
        </div>



    </div>
</div>
</div>
    <style>
        .lds-ring {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }
        .lds-ring div {
            box-sizing: border-box;
            display: block;
            position: absolute;
            width: 51px;
            height: 51px;
            margin: 6px;
            border: 6px solid #333;
            border-radius: 50%;
            animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            border-color: #333 transparent transparent transparent;
        }
        .lds-ring div:nth-child(1) {
            animation-delay: -0.45s;
        }
        .lds-ring div:nth-child(2) {
            animation-delay: -0.3s;
        }
        .lds-ring div:nth-child(3) {
            animation-delay: -0.15s;
        }
        @keyframes lds-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    @push('scripts')
        <script>window.csrfToken = '{{csrf_token()}}'; window.cartUrl = '{{url('tienda/cart')}}';</script>

        <!----details-product-slider--->
        <!-- Include the Etalage files -->
        <script>
            //Variables globales
            var price = {{$product->sale_price}};
            var horas;
            var total;
            //Funciones
            function getDiff(start,end) {
                let date1 = new Date(2000, 0, 1,   start.split(':')[0],  start.split(':')[1]); // 9:00 AM
                let date2 = new Date(2000, 0, 1,  end.split(':')[0], end.split(':')[1]); // 5:00 PM
                /*if (date2 < date1) {
                    date2.setDate(date2.getDate() + 1);
                }*/
                var diff = date2 - date1;
                return Math.floor(diff / 1000 / 60);
            }
            function getDifference() {
                let diff = getDiff($("#start").val(),$("#end").val());
                return diff;
            }
            function calculoTotal() {
                horas = Math.floor(getDifference() /60);
                total = horas*price;
                $("#horas").html(horas);
                $("#total").html(total);
                //console.log(horas);

            }


            $("#start").bind('keyup mouseup change', function () {
                calculoTotal();
            });
            $("#end").bind('keyup mouseup change', function () {
                calculoTotal();
            });


            function errMsg(msg) {
                toastr.error(msg);
                return;
                $("#msg").show();
                $("#msg p").html(`${msg}`);
            }
            $('#btn-add').click(function() {
                $("#msg").hide();

                let time = getDifference();
                let fecha = $("#fecha").val();
                let start_time = $("#start").val();
                let end_time = $("#end").val();
                let address = $("#address").val();
                let sale_price = price;
                if (!time || time < 60) {
                    errMsg('Especifica horas válidas de inicio y final');
                    return;
                }
                if (!fecha) {
                    errMsg('Especifica una fecha válida');
                    return;
                }
                options = {
                    "alquiler": true,
                    "start_time": start_time,
                    "end_time": end_time,
                    "horas" : horas,
                    "total" : total,
                    "address": address,
                    "fecha" : fecha,
                    "sale_price" : sale_price
                }
                id = {{$product->id}};
                cartAction('add','{{$product->code}}',{{$product->id}},1,options);

            });
            function load(action) {
                if (action=='start') {
                    $("#btn-add").hide();
                    $("#loader").show();
                }else if(action=='end') {
                    $("#btn-add").show();
                    $("#loader").hide();
                }

            }

        </script>
        <link rel="stylesheet" href="{{asset('frontend/css/etalage.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap-datepicker3.min.css')}}">
        <script src="{{asset('frontend/js/jquery.etalage.min.js')}}"></script>
        <script src="{{asset('frontend/js/bootstrap-datepicker.min')}}"></script>
        <!-- Include the Etalage files -->
        <script>
            jQuery(document).ready(function($){

                $('#etalage').etalage({
                    thumb_image_width: 300,
                    thumb_image_height: 400,

                    show_hint: true,
                    click_callback: function(image_anchor, instance_id){
                        alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
                    }
                });
                // This is for the dropdown list example:
                $('.dropdownlist').change(function(){
                    etalage_show( $(this).find('option:selected').attr('class') );
                });

            });


        </script>
        <!----//details-product-slider--->
    @endpush
    @endsection