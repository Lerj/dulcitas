@extends('layouts.admin.template')

@section('contenido')
    <div class="card">
        <div class="card-header">
            <h3>Menú de opciones</h3>
        </div>
        <div class="card-body">
        <div class="row">
            <div class="col-sm-3 col-xs-12">
                
                <a href="{{route('clientarea.payment.index')}}" class="btn btn-danger btn-lg" role="button">
                    <span class="fa fa-dollar-sign" style="font-size:50px;"></span> 
                    <br/>
                    Pagos
                </a>
            
                </div>
            </div>
        </div>
        </div>

    </div>



@endsection