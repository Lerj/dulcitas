@extends('layouts.clientarea.template')
@section('contenido')
    
    <div>
        <div class="col-sm-12">
            <h3>Crear nueva categoría</h3>
            @if(count($errors->all())>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error) 
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        {!! Form::open(['method'=>'POST','url'=>'admin/categorias']) !!}
        {{Form::token()}}

                <div class="col-sm-12 col-xs-12">
                    <div class="form-group{{ $errors->has('categoria') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <label for="user">Categoría</label><input type="text" value="{{old('categoria')}}" class="form-control" placeholder="Usuario" name="categoria">
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-primary">Agregar</button>
                
                <a href="{{url('admin/categorias')}}" class="btn btn-danger">Regresar</a>
            </div>

                
            
        {!! Form::close() !!}

        </div>
    </div>

    
@endsection
