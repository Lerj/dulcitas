@extends('layouts.clientarea.template')
@section('contenido')
    
    <div>
        <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12">
            <h3>Editar categoría {{$categoria->name}}</h3>
            @if(count($errors->all())>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error) 
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
        {!! Form::open(['method'=>'PATCH','url'=>['admin/categorias',$categoria->id]]) !!}
       
        
        {{Form::token()}}
            <div class="col-sm-12 col-xs-12">
                <div class="form-group{{ $errors->has('categoria') ? ' has-error' : '' }}">
                    <div class="input-group">
                        <label for="user">Categoría</label><input type="text" value="{{$categoria->categoria}}" class="form-control" placeholder="Categoria" name="categoria">
                    </div>
                </div>
            </div>


                

                
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary">Editar</button>
                
                    <a href="{{url('admin/categorias')}}" class="btn btn-danger">Regresar</a>
                </div>
            </div>

                
     
            
        {!! Form::close() !!}

        </div>
    </div>

    
@endsection