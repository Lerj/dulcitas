@extends('layouts.clientarea.template')
@section('contenido')   

        @if(Session::has('alert'))
            <div class="alert alert-{{Session::get('alert')['tipo']}}">
                <p>{{Session::get('alert')['mensaje']}}</p>
            </div>
        @endif

    <h1>Lista de entradas</h1>

    <table class="table table-responsive table-bordered table-condensed table-striped table-hover">
        <thead>
            <th>Evento</th>
            <th>Lugar</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>Estado</th>
            <th>Tipo de Entrada</th>
            <th>Acciones</th>
      
            
        </thead>
        <tbody>
            @foreach($entradas as $fila) 

                    <tr>
                        
                        <td>{{$fila->entrada->evento["nombre"]}}</td>
                        <td>{{$fila->entrada->evento["lugar"]}}</td>
                        <td>{{$fila->entrada->evento["fecha"]}}</td>
                        <td>{{$fila->entrada->evento["hora"]}}</td>
                        <td>{{$fila->entrada["tipo"]}}</td>

                        <td>{{$fila->status == 1 ? 'Activa' : 'Canjeada'}}</td>
                   
                   
                    
                    <td>
                        <a class="btn btn-primary" href="{{action('clientarea\ticketController@print',['id'=>$fila->id])}}">Imprimir Entrada</a>
                        
                    </td>

                </tr>
                
            @endforeach

        </tbody>

    </table>

    
 

    @push('scripts')
        	<script>

            </script>
    @endpush
@endsection