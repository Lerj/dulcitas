@extends('layouts.clientarea.template')

@section('contenido')
    @if(\Session::has('info'))
    <div class="alert alert-success">
        <p>{{Session::get('info')}}</p>
    </div>
    @endif
    <h1>Lista de Eventos Disponibles</h1>

    @foreach($eventos as $evento)

    <div class="col-sm-3">

        <div class="panel panel-primary">
            <div class="card-heading">
                <h4>Ventticket</h4>
            </div>

            <div class="card-body">
                <p><strong>Categoría: </strong>{{$evento->categoria["categoria"]}}</p>
                <p><strong>Nombre:</strong> {{$evento->nombre}}</p>
                <p><strong>Lugar:</strong> {{$evento->lugar}}</p>
                <p><strong>Fecha:</strong> {{$evento->fecha}}</p>
                <p><strong>Hora:</strong> {{$evento->hora}}</p>
                <a href="{{route('clientarea.event.show',['id'=>$evento->id])}}" class="btn btn-success text-center" style="margin:auto;">Ver</a>
            </div>
        </div>
    </div>
    @endforeach

    <!--div class="col-sm-12">
        <table class="table table-hover table-striped table-condensed">
            <thead>
              
                <th>Nombre</th>
                <th>Lugar</th>
                <th>Fecha</th>
                <th>Hora</th>
                <th>Categoría</th>
                <th>Acciones</th>
            </thead>

            <tbody>
                @foreach($eventos as $evento)
                    <tr>
                        
                        <td>{{$evento->nombre}}</td>
                        <td>{{$evento->lugar}}</td>
                        <td>{{$evento->fecha}}</td>
                        <td>{{$evento->hora}}</td>
                        <td>{{$evento->categoria["categoria"]}}</td>
                        <td>
                            <a href="{{route('clientarea.event.show',['id'=>$evento->id])}}" class="btn btn-primary">Ver</a>
                        </td>

                      
                    </tr>

                    @include('admin.evento.modal')
                @endforeach
            </tbody>

        </table>

        {{$eventos->render()}}
    </div-->

@endsection