@extends('layouts.clientarea.template')

@section('contenido')

    <h1 class="text-center">Ver Evento</h1>

    <div class="col-sm-12">

        <div class="panel panel-primary">
            <div class="card-heading">
                <h4>Ventticket</h4>
            </div>

            <div class="card-body">
                <p><strong>Categoría: </strong>{{$evento->categoria["categoria"]}}</p>
                <p><strong>Nombre:</strong> {{$evento->nombre}}</p>
                <p><strong>Lugar:</strong> {{$evento->lugar}}</p>
                <p><strong>Fecha:</strong> {{$evento->fecha}}</p>
                <p><strong>Hora:</strong> {{$evento->hora}}</p>
                
                <h2>Entradas:</h2>
                <table class="table table-responsive table-bordered table-condensed table-striped table-hover">
                <thead>
                    <th>ID</th>
                    <th>Tipo</th>
                    <th>Costo</th>
                    <th>Disponibilidad</th>
                    <th>Acciones</th>
                    
                </thead>
                <tbody>
                    @foreach($evento->entradas as $fila) 
        
                            <tr>
                            <td>{{$fila->id}}</td>
                            <td>{{$fila->tipo}}</td>
                            <td>{{$fila->costo}} Bsf.</td>
                            <td>{{$fila->disponibilidad - $fila->usuarios->count()}}/{{$fila->disponibilidad}}</td>
                            
                            <td>
                                @if($fila->disponible())
                                    <a class="btn btn-primary" href="{{action('clientarea\eventoController@venta',['evento'=>$evento->id,'entrada'=>$fila->id])}}">Comprar</a>
                                @else
                                    <strong>AGOTADA</strong>
                                @endif
                               
                            </td>
        
                        </tr>
                        
                    @endforeach
        
                </tbody>
        
            </table>
            </div>



        </div>
    </div>
@endsection