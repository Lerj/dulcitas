@extends('layouts.admin.template')

@section('contenido')
    <div class="card card-primary">
        
        <div class="card-body">
  
            @if(Session::has('alert')) 

                <div class="alert alert-{{Session::get('alert')['tipo']}}" id="alert">
                    {{Session::get('alert')['mensaje']}}
                </div>
                <script>
                    
                </script>
            @endif
            <h1>Lista de pagos</h1>

            <table class="table table-bordered table-condensed table-striped table-hover table-responsive">
                <thead>
                    <th>ID</th>
                    <th>Factura N°</th>
                    <th>Usuario</th>
                    <th>Fecha de Solicitud</th>
                    <th>Estado</th>
                    <th>Productos</th>
                    <th>Acciones</th>
                    
                </thead>
                <tbody>
                    @foreach($pagos as $fila)

                        <tr>
                            <td>{{$fila->id}}</td>
                            <td>{{$fila->invoice["code"]}}</td>
                            <td>{{$fila->user["name"]}}</td>
                            <td>{{\Carbon\Carbon::parse($fila->created_at)->toDateTimeString()}} ({{\Carbon\Carbon::parse($fila->created_at)->diffForHumans()}})</td>
                            <td>{{$status[$fila->status]}}</td>
                            <td>{{\App\InvoiceDetails::where('invoice_id',$fila->invoice_id)->count()}}</td>

                            <td>
                                <a href="{{route('clientarea.payment.show',$fila->id)}}" alt="Ver" aria-labelledby="Ver" class="btn btn-success"><i class="fa fa-eye"></i></a>

                                <a href="{{route('clientarea.payment.facturapdf',$fila->id)}}" alt="Ver" aria-labelledby="" class="btn btn-warning">PDF</a>
                                @if($fila->status==1) 
                                    <button class="btn btn-primary" onclick="$('#consignar-{{$fila->id}}').modal('show')">Consignar</button>
                                @endif

                                @if($fila->status==2 || $fila->status==4)
                                    <button class="btn btn-primary" onclick="$('#info-{{$fila->id}}').modal('show')">Ver</button>
                                @endif

                                @if($fila->status==3)
                                    <button class="btn btn-primary" onclick="$('#info-{{$fila->id}}').modal('show')">Ver</button>
                                @endif

                            </td>
                            
                            
                    
                        </tr>
                        
                        @include('clientarea.payments.modal')
                    @endforeach

                </tbody>

            </table>
            
            @push('scripts')

                <script>
                    function paypal(id) {
                        confirm('[ATENCIÓN]\nSe redirigirá a una página para procesar el pago vía paypal\nDebe regresar a esta para confirmar la transacción');
                        $("#paypal-"+id).show().find('input').prop('name','cuenta_paypal');
                        $("#banco-"+id).hide();
                    }
                    function banco(id) {
                        confirm('[ATENCIÓN]\nEstos son los datos para el depósito bancario, debe realizar el pago y escribir el número de referencia');
                        $("#banco-"+id).show().find('input').prop('name','referencia_bancaria');
                        $("#paypal-"+id).hide();
                    }
                </script>
            @endpush
        </div>
    </div>
@endsection