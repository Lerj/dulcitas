@extends('layouts.admin.template')

@section('contenido')
    <div class="card">
        <div class="card-header">
            <h3 class="text-center">Confirmación de pago</h3>
        </div>


        <div class="card-body">

        <table class="table table-bordered">
           
            <tbody>
                <tr>
                    <td>Evento</td>
                    <td>{{session('pago')['evento']}}</td>
                </tr>
                <tr>
                    <td>Razón de pago:</td>
                    <td>{{session('pago')['razon_pago']}}</td>
                </tr>
                <tr>
                    <td>Total:</td>
                    <td>{{session('pago')['total']}}Bsf.</td>
                </tr>
            </tbody>


            
        </table>
            <div class="float-md-right">
                    <a href="{{action('clientarea\paymentController@create')}}" class="btn btn-primary" >Confirmar Pago</a>
            </div>
        </div>
    </div>


    <style>
        .table tbody tr td:nth-child(2) {
            text-align: right;
        }
        .table tbody tr td {
            font-weight: bold;
        }
    </style>
@endsection