<div class="modal fade" aria-hidden="true"
role="dialog" tabindex="-1" id="consignar-{{$fila->id}}">

	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title">Consignar Pago</h4>

                <button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
			</div>
			<div class="modal-body">
                <h3>Factura N° {{$fila->invoice["code"]}}</h3>

                <p>Total: <strong>{{$fila->total}}Bsf.</strong></p>
                <p>Fecha de solicitud: <strong>{{$fila->fecha_solicitud}} ({{\Carbon\Carbon::parse($fila->fecha_solicitud)->diffForHumans()}})</strong></p>


                <hr>

                <div class="row">


                    <a  class="btn btn-primary" onclick="banco({{$fila->id}})">Pagar con Cuenta Bancaria</a>
                </div>
                <br>

               {{Form::open(['action'=>['clientarea\paymentController@store',$fila->id],'method'=>'get'])}}

                <div id="paypal-{{$fila->id}}" style="display: none;">



                    <div class="input-group">
                        <input type="email" class="form-control" placeholder="Introduzca la cuenta paypal">
                        <span class="input-group-btn"><button type="submit" class="btn btn-primary" >Confirmar Pago</button></span>
                    </div>

                </div>

                  <div id="banco-{{$fila->id}}" style="display: none;">


                    <div class="form-group">
                        <h3>Banco provincial</h3>
                        <!--img src="{{asset('img/logoVzla.png')}}" width="150px" height="50px" alt=""-->

                        <h3><strong>Páguese a nombre de:</strong></h3>
                        <p><strong>Nombre:</strong> Promulara y J Mary C.A</p>
                        <p><strong>Cuenta: </strong>Corriente</p>
                        <p><strong>Número de Identificación:</strong> J-9552993-6</p>
                        <p><strong>Número de Cuenta:</strong> 01082456790100173251</p>


                    </div>
                    <div class="input-group">



                        <input type="text" class="form-control" placeholder="Introduzca el numero de referencia del depósito">
                        <span class="input-group-btn"><button type="submit" class="btn btn-primary" >Confirmar Pago</button></span>

                    </div>

                </div>

                 {{Form::Close()}}



			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>


</div>


<div class="modal fade modal-slide-in-right" aria-hidden="true"
role="dialog" tabindex="-1" id="info-{{$fila->id}}">
	
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title">Consignar Pago</h4>

                <button type="button" class="close" data-dismiss="modal"
				aria-label="Close">
                     <span aria-hidden="true">×</span>
                </button>
			</div>
			<div class="modal-body">
                <p>Razón de pago: <strong>{{$fila->razon_pago}}</strong></p>
                <p>Total: <strong>{{$fila->total}}Bsf.</strong></p>
                <p>Fecha de solicitud: <strong>{{$fila->fecha_solicitud}} ({{\Carbon\Carbon::parse($fila->fecha_solicitud)->diffForHumans()}})</strong></p>
                <p>Estado: <strong>{{$status[$fila->status]}}</strong> </p>
                @if($fila->status==3)
                    <div class="form-group">

                        <h3><strong>Páguese a nombre de:</strong></h3>
                        <p><strong>Nombre:</strong> Promulara y J Mary C.A</p>
                        <p><strong>Cuenta: </strong>Corriente</p>
                        <p><strong>Número de Identificación:</strong> J-9552993-6</p>
                        <p><strong>Número de Cuenta:</strong> 01082456790100173251</p>

                    </div>
                @endif

                @if($fila->cuenta)
                    <p>Pagado por Cuenta bancaria, Referencia: <strong>{{$fila->cuenta["referencia"]}}</strong></p>
                    <p>el <strong>{{$fila->fecha_pago}} ({{\Carbon\Carbon::parse($fila->fecha_pago)->diffForHumans()}})</strong></p>
                @endif
                @if($fila->status==4)
                    <div class="alert alert-danger">
                        <h4><i class="fa fa-warning"></i> Pago negado</h4>
                    </div>

                    <p><strong>Razón: </strong>{{$fila->razon_negado}}</p>
                    <hr>
                    {{Form::open(['action'=>['clientarea\paymentController@store',$fila->id],'method'=>'get'])}}


                    <a  class="btn btn-primary" onclick="$('#pagar-{{$fila->id}}').show(); banco({{$fila->id}}); ">Pagar con Cuenta Bancaria</a>

                    <div id="pagar-{{$fila->id}}" style="display: none;">
                        <div class="form-group">
                            <h3>Banco provincial </h3>
                        <!--img src="{{asset('img/logoVzla.png')}}" width="150px" height="50px" alt=""-->

                            <h3><strong>Páguese a nombre de:</strong></h3>
                            <p><strong>Nombre:</strong> Promulara y J Mary C.A</p>
                            <p><strong>Cuenta: </strong>Corriente</p>
                            <p><strong>Número de Identificación:</strong> J-9552993-6</p>
                            <p><strong>Número de Cuenta:</strong> 01082456790100173251</p>

                        </div>
                        <div class="input-group">



                            <input type="text" class="form-control" placeholder="Introduzca el numero de referencia del depósito">
                            <span class="input-group-btn"><button type="submit" class="btn btn-primary" >Confirmar Pago</button></span>

                        </div>

                    </div>

                    {{Form::Close()}}
                @endif



                
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				
			</div>
		</div>
	</div>
</div>
<style>
    .modal-body {
        overflow-x: auto;
    }

</style>
