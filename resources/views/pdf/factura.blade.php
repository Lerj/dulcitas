<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Recibo del pedido </title>
    
    <style>

    .invoice-box{
        
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        width: 500px;
        font-size:16px;
        line-height:24px;
        font-family: Arial, sans-serif;
        color:#555;
    }
    
    .invoice-box table{
        width:100%;
        
        text-align:left;
    }
    
    .invoice-box table td{
        padding:5px;
        
        
    }
    
    .invoice-box table tr td:nth-child(2){
        
        text-align: left;
    }
    .heading {
        width: 100%;
    }
    
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }
    
    
    .factura-cliente {
        border: 1px solid black;
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box" style="width: 100%">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr class="top">
                <td colspan="6">
                    <table>
                        <tr>
                            <td class="title">
                                
                            </td>
                            
                            <td style="text-align: right">
                                Dulcitas .C.A <br>
                                
                                RIF. J9552993-2
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information ">
                <td colspan="6">
                    <table>
                        <tr>
                            <td>
                                <b>Factura N°:</b> {{$invoice->code}}
                                

                                
                                
                                
                            </td>
                        </tr>
                        <tr class="factura-cliente">
                             
                            <td>
                                <b>Nombre y Apellido: </b> <span style="">{{Auth::user()->persona["name"]}} {{Auth::user()->persona["lastname"]}}</span><br>
                                <!--b>Persona:</b> <span style="">entrada->personacliente</span><br-->
                                
                                <b>Cédula:</b> <span style="">{{Auth::user()->persona["cedula"]}}</span><br>
                                
                                
                                
                            </td>
                            
                            
                        </tr>
                    </table>
                </td>
            </tr>
          
          
            
            <tr class="heading">
                <td style="width:10px">
                    Nombre
                </td>
                
                <td width="">
                    Cantidad
                </td>
                <td>Monto</td>

                
                
            </tr>
            
           
            
               @foreach($invoice->details as $de)
                <tr>
                    
                    <td>{{$de->product->name_product}} </td>
                    <td>{{$de->quantity}}</td>
                    <td>{{$de->price}} BS.S</td>
                    
                </tr>
                   @endforeach


<tr>
    <td></td>
    <td></td>
    <td><strong>SubTotal:</strong> {{$invoice->subtotal}} Bs.S</td></tr>
            <tr>
                <td></td>
                <td></td>
                <td><strong>IVA ({{$invoice->iva}}%):</strong> {{$invoice->subIva}} Bs.S</td></tr>
            
            <tr>

                <td></td>
                <td></td>
                <td><strong>Total:</strong> {{$invoice->total}}</td>
            </tr>
        </table>
      



    </div>
</body>
</html>