<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Factura de entrada</title>
    
    <style>
        body {
            max-width:800px;
        }
    .invoice-box{
        
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        
        font-size:16px;
        line-height:24px;
        font-family: Arial, sans-serif;
        color:#555;
    }
    
    .invoice-box table{
        width:100%;
        
        text-align:left;
    }
    
    .invoice-box table td{
        padding:5px;
        
        
    }
    
    .invoice-box table tr td:nth-child(2){
        
        text-align: center;
    }
    
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }
    
    
    .factura-cliente {
        border: 1px solid black;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr class="top">
                <td colspan="6">
                    <table>
                        <tr>
                            <td class="title">
                                
                            </td>
                            
                            <td style="text-align: right">
                                PromuLara ,C.A <br>
                                
                                RIF. J9552993-2
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information ">
                <td colspan="4">
                    <table>
                        <tr>
                            <td>
                                <b>Codigo N°:</b> {{$entrada->entrada->evento['id'].'-'.$entrada->entrada["id"].'-'.$entrada->codigo}}
                                

                                
                                
                                
                            </td>
                        </tr>
                        <tr class="factura-cliente">
                             
                            <td>
                                <b>Nombre y Apellido: </b> <span style="">{{Auth::user()->persona["name"]}} {{Auth::user()->persona["lastname"]}}</span><br>
                                <!--b>Persona:</b> <span style="">entrada->personacliente</span><br-->
                                
                                <b>Cédula:</b> <span style="">{{Auth::user()->persona["cedula"]}}</span><br>
                                
                                
                                
                            </td>
                            
                            
                        </tr>
                    </table>
                </td>
            </tr>
          
          
            
            <tr class="heading">
                <td style="width:10px">
                    Nombre
                </td>
                
                <td width="">
                    Cantidad
                </td>
                <td>Monto</td>

                
                
            </tr>
            
           
            
                  
                <tr>
                    
                     
                    <td>{{$entrada->entrada->evento["nombre"]}}</td>
                    <td>{{$entrada->entrada["tipo"]}}</td>
                    <td>{{$entrada->entrada->evento["lugar"]}}</td>
                    <td>{{$entrada->entrada->evento["fecha"]}}</td>
                        
                    <td>{{$entrada->entrada->evento["hora"]}}</td>
                        
                        
                        
                    
                    <td>{{$entrada->entrada["costo"]}} Bsf.</td>
                   
                    
                </tr>
  
        </table>
      
        <div style="float: right; margin-top: 90px;">
                <img src="data:image/png;base64,{{DNS1D::getBarcodePNG($entrada->entrada->evento['id'].'-'.$entrada->entrada["id"].'-'.$entrada->codigo, 'C39+',1.2,33,array(1,1,1), true)}}" alt="barcode"   />        
        </div>

        <div style="float: left;">
            <img src="data:image/png;base64,{{DNS2D::getBarcodePNG($entrada->entrada->evento['id'].'-'.$entrada->entrada["id"].'-'.$entrada->codigo, 'QRCODE',5,5)}}" alt="barcode"   />                
        </div>
    </div>
</body>
</html>