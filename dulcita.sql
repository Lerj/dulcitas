-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 02-02-2019 a las 11:21:09
-- Versión del servidor: 5.7.24-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dulcita`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `categoria` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`, `descripcion`) VALUES
(1, 'Infantiles', 'Productos para fiestas infantiles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `iva` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoice`
--

INSERT INTO `invoice` (`id`, `user_id`, `code`, `created_at`, `updated_at`, `iva`) VALUES
(1, 1, 10000, '2019-01-20 19:04:38', '2019-01-20 19:04:38', 16),
(2, 1, 10000, '2019-01-20 19:04:48', '2019-01-20 19:04:48', 16),
(3, 1, 10000, '2019-01-20 19:04:58', '2019-01-20 19:04:58', 16),
(4, 1, 10000, '2019-01-20 19:05:09', '2019-01-20 19:05:09', 16),
(5, 1, 10000, '2019-01-20 19:05:39', '2019-01-20 19:05:39', 16),
(6, 2, 10000, '2019-01-20 19:18:35', '2019-01-20 19:18:35', 16),
(7, 2, 10000, '2019-01-20 19:18:37', '2019-01-20 19:18:37', 16),
(8, 2, 10000, '2019-01-20 20:22:23', '2019-01-20 20:22:23', 16),
(9, 2, 10000, '2019-01-20 20:25:43', '2019-01-20 20:25:43', 16),
(10, 1, 0, '2019-02-02 17:29:24', '2019-02-02 17:29:24', 16),
(11, 1, 0, '2019-02-02 17:43:11', '2019-02-02 17:43:11', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` int(11) NOT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `invoice_id`, `products_id`, `quantity`, `price`) VALUES
(1, 3, 1, 4, 200),
(2, 4, 1, 4, 200),
(3, 5, 1, 4, 200),
(4, 6, 1, 6, 200),
(5, 7, 1, 6, 200),
(6, 8, 1, 5, 200),
(7, 9, 1, 17, 200),
(8, 10, 1, 6, 200),
(9, 11, 1, 1, 200);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_06_23_162922_create_categorias_table', 1),
(2, '2018_06_23_162922_create_invoice_details_table', 1),
(3, '2018_06_23_162922_create_invoice_table', 1),
(4, '2018_06_23_162922_create_pago_instapago_table', 1),
(5, '2018_06_23_162922_create_password_resets_table', 1),
(6, '2018_06_23_162922_create_payments_table', 1),
(7, '2018_06_23_162922_create_permission_role_table', 1),
(8, '2018_06_23_162922_create_permissions_table', 1),
(9, '2018_06_23_162922_create_persona_table', 1),
(10, '2018_06_23_162922_create_products_table', 1),
(11, '2018_06_23_162922_create_role_user_table', 1),
(12, '2018_06_23_162922_create_roles_table', 1),
(13, '2018_06_23_162922_create_status_table', 1),
(14, '2018_06_23_162922_create_users_table', 1),
(15, '2018_06_23_162924_add_foreign_keys_to_pago_instapago_table', 1),
(16, '2018_06_23_162924_add_foreign_keys_to_payments_table', 1),
(17, '2018_06_23_162924_add_foreign_keys_to_persona_table', 1),
(18, '2018_06_23_162924_add_foreign_keys_to_products_table', 1),
(19, '2018_06_25_213555_create_shoppingcart_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_instapago`
--

CREATE TABLE `pago_instapago` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `referencia` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `pago_instapago`
--

INSERT INTO `pago_instapago` (`id`, `invoice_id`, `referencia`, `voucher`) VALUES
(1, 5, '722575', NULL),
(2, 6, '535841', NULL),
(3, 7, '455161', NULL),
(4, 8, '544406', NULL),
(5, 9, '945050', NULL),
(6, 10, '269740', NULL),
(7, 11, '838070', '\r\n<table style=\"background-color: white;\">\r\n    <tbody>\r\n        <tr>\r\n            <td>\r\n                <div style=\"border: 1px solid #222; padding: 9px; text-align: center; max-width:255px\" id=\"voucher\">\r\n\r\n                    <style type=\"text/css\">\r\n                        .normal-left {\r\n                            font-family: Tahoma;\r\n                            font-size: 7pt;\r\n                            text-align: left;\r\n                        }\r\n\r\n                        .normal-right {\r\n                            font-family: Tahoma;\r\n                            font-size: 7pt;\r\n                            text-align: right;\r\n                        }\r\n\r\n                        .big-center {\r\n                            font-family: Tahoma;\r\n                            font-size: 9pt;\r\n                            text-align: center;\r\n                            font-weight: 900;\r\n                        }\r\n\r\n                        .big-center-especial {\r\n                            font-family: Tahoma;\r\n                            font-size: 9pt;\r\n                            text-align: center;\r\n                            font-weight: 900;\r\n                            letter-spacing: .9em;\r\n                        }\r\n\r\n                        .big-left {\r\n                            font-family: Tahoma;\r\n                            font-size: 9pt;\r\n                            text-align: left;\r\n                            font-weight: 900;\r\n                        }\r\n\r\n                        .big-right {\r\n                            font-family: Tahoma;\r\n                            font-size: 9pt;\r\n                            text-align: right;\r\n                            font-weight: 900;\r\n                        }\r\n\r\n                        .normal-center {\r\n                            font-family: Tahoma;\r\n                            font-size: 7pt;\r\n                            text-align: center;\r\n                        }\r\n\r\n                        #voucher td {\r\n                            padding: 0;\r\n                            margin: 0;\r\n                        }\r\n                    </style>\r\n                    <div id=\"voucher\">\r\n                        <table>\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"normal-center\">COPIA - CLIENTE</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"big-center-especial\">\r\n                                    <br />\r\n                                    BANESCO\r\n                                </td>\r\n                            </tr>\r\n\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"big-center\">\r\n                                    <br />\r\n                                    \r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" style=\"height: 8px;\"></td>\r\n                            </tr>\r\n\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"normal-left\">TECNOLOGIA INSTAPAGO</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"normal-left\">DEMOSTRACI&#211;N</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"normal-left\">J-000000000</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" style=\"height: 8px;\"></td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"2\" class=\"normal-left\">000000000000</td>\r\n                                <td colspan=\"2\" class=\"normal-right\">000000000000</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"1\" class=\"normal-left\">FECHA:</td>\r\n                                <td colspan=\"3\" class=\"normal-left\">00/00/00 00:00:00 PM</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"1\" class=\"normal-left\">NRO CUENTA:</td>\r\n                                <td colspan=\"2\" class=\"normal-left\">000000******0000    </td>\r\n                                <td class=\"normal-right\">&#39;0&#39;</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td class=\"normal-left\">NRO. REF.:</td>\r\n                                <td class=\"normal-left\">000000</td>\r\n                                <td class=\"normal-right\">LOTE:</td>\r\n                                <td class=\"normal-right\">000</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"1\" class=\"normal-left\">APROBACION: </td>\r\n                                <td colspan=\"3\" class=\"normal-left\">000000</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"1\" class=\"normal-left\">SECUENCIA:</td>\r\n                                <td colspan=\"3\" class=\"normal-left\"></td>\r\n                            </tr>\r\n\r\n                            <tr>\r\n                                <td colspan=\"4\" style=\"height: 8px;\"></td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"big-center\">\r\n                                    <br />\r\n                                    MONTO BS.  0,00\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" style=\"height: 8px;\"></td>\r\n                            </tr>\r\n                            <tr style=\"margin-top: 10px;\">\r\n                                <td colspan=\"4\" class=\"big-center\">RIF: J-000000000</td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" style=\"height: 8px;\"></td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"normal-left\">\r\n                                    <b>\r\n                                        <br />\r\n                                    </b>\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"4\" class=\"normal-left\">\r\n                                    <br />debito\r\n                                </td>\r\n                            </tr>\r\n                            <tr>\r\n                                <td colspan=\"1\" class=\"normal-left\">ID:</td>\r\n                                <td colspan=\"3\" class=\"normal-left\">000000000000000000</td>\r\n                            </tr>\r\n                        </table>\r\n                    </div>\r\n                </div>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n\r\n\r\n\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `fecha_pago` datetime DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `total` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `voucher` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `persona`
--

CREATE TABLE `persona` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cedula` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `users_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `persona`
--

INSERT INTO `persona` (`id`, `name`, `lastname`, `phone`, `address`, `cedula`, `birthday`, `users_id`) VALUES
(1, 'Luis', 'Rodriguez', NULL, NULL, '24549758', NULL, 1),
(2, 'Luis', 'Rodriguez', NULL, NULL, '24549758', NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name_product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `stock` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `sale_price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name_product`, `descripcion`, `categories_id`, `code`, `image`, `type`, `stock`, `status_id`, `sale_price`, `created_at`, `updated_at`) VALUES
(1, 'Helado', 'Helado', 1, '100000', 'product-2.jpg', NULL, '90', 1, 200, '2019-01-20 18:51:00', '2019-01-20 18:51:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'root', 'Root', 'Dueño del sistema', NULL, NULL),
(2, 'admin', 'Administrador', 'Administrador del sistema', NULL, NULL),
(3, 'cliente', 'Cliente', 'Cliente del sistema', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(3, 2),
(2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE `status` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Activo', NULL, NULL),
(2, 'Suspendido', NULL, NULL),
(3, 'Eliminado', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `user`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin@dulcitas.com', '$2y$10$jtLGbCohMdiS7r3Z8FDW7OotjNWE93ZVVQI9M9awLwbc.lP99GXf2', 'lhbphzyU0WHEBtHvmJtaLqvfPMp0SW2S16MjSYNTjBSAU89U7C9MF2JVmAUe', NULL, NULL),
(2, 'cliente', 'cliente', 'cliente@dulcitas.com', '$2y$10$x/fTmHYmhOrxozwJzTsVZ.nAtUGk8dxWIO8Wtf7dPHb0zjvf5jWi.', 'rE8aOpcUKve8MB2iXqLw0X5WMUzxXdY5TQGD0KmgjoNHU2fPxYOL7MKtdJKz', NULL, NULL),
(3, 'Prueba', 'pruebita', 'prueba@gmail.com', '$2y$10$2INVh4dq.sj1NJZd7vLty.sfZp3nS/O3hx/9gamYqdllQv1D7KDl.', NULL, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pago_instapago`
--
ALTER TABLE `pago_instapago`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pago_instapago_invoice_id_foreign` (`invoice_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `usr_id` (`user_id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indices de la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `persona`
--
ALTER TABLE `persona`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_categories_id_foreign` (`categories_id`),
  ADD KEY `products_status_id_foreign` (`status_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_user_unique` (`user`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `pago_instapago`
--
ALTER TABLE `pago_instapago`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `persona`
--
ALTER TABLE `persona`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pago_instapago`
--
ALTER TABLE `pago_instapago`
  ADD CONSTRAINT `pago_instapago_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `persona`
--
ALTER TABLE `persona`
  ADD CONSTRAINT `persona_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_categories_id_foreign` FOREIGN KEY (`categories_id`) REFERENCES `categorias` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
