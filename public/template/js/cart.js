toastr.options = {
    "debug": false,
//"positionClass": "toast-bottom-full-width",
    "onclick": null,
    "fadeIn": 300,
    "fadeOut": 1000,
    "timeOut": 5000,
    "extendedTimeOut": 1000
}
function cartAction(action,product_code,id,quantity,options) {
    var queryString = {};
    queryString.action = action;
    queryString.id = id;
    queryString._token = window.csrfToken;
    queryString.options = options;
    if(action) {
        switch(action) {
            case "add":
                queryString.code = product_code;
                if (!quantity) {
                    queryString.quantity = $("#qty_" + product_code).val();
                }else {
                    queryString.quantity = quantity;
                }
                break;
            case "remove":
                queryString.code = product_code;
                break;
            case "empty":

                break;
        }
    }
    jQuery.ajax({
        url: window.cartUrl,
        dataType: 'json',
        data:queryString,
        type: "POST",
        success:function(data){
            console.log(data);
            action = data.action;
            //$("#cart-item").html(data);
            if (data.status=="error") {
                errMsg(data.msg);
                return;
            }
            if(action) {
                switch(action) {
                    case "add":
                        toastr.success('Se agregó al carrito el producto \''+data.product.name_product+'\'');
                        setTimeout(function() { location.reload();},1500);
                        $("#add_"+product_code).hide();
                        $("#added_"+product_code).show();
                        break;
                    case "remove":
                        toastr.success('Se eliminó del carrito el producto exitósamente');
                        setTimeout(function() { location.reload();},1500);
                        $("#add_"+product_code).show();
                        $("#added_"+product_code).hide();
                        break;
                    case "empty":
                        toastr.success('Se limpió el carrito de compras');
                        setTimeout(function() { location.reload();},1500);

                        $(".btnAddAction").show();
                        $(".btnAdded").hide();
                        break;
                }
            }
        },
        error:function (data){


            //$("#cart-item").html(data);}
            console.log(data);
            data = JSON.parse(data.responseText)
            let mensaje = data.msg;

            if (mensaje)
                errMsg(mensaje);



        }
    });
}
