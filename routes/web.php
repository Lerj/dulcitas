<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('/admin/users',"UsersController");

Route::group(['as' => 'admin.','prefix'=>'admin'],function(){
	Route::resource('categorias','CategoriaController');
    Route::resource('products','ProductsController');

});

Route::group(['route' => 'tienda', 'as' => 'tienda.', 'name' =>'tienda', 'prefix' =>'tienda'],function() {


	Route::get('/','tiendaController@index')->name('index');
	Route::get('products/{id}','tiendaController@show')->name('product.show');
	Route::get('cart','tiendaController@getCart')->name('cart');
	Route::post('cart/add','tiendaController@add')->name('cart.add');
	Route::post('cart/del/{rowId}','tiendaController@del')->name('cart.del');
	Route::get('products','tiendaController@products')->name('products');
	Route::get('checkout','tiendaController@checkout')->name('checkout');
	Route::get('payment','tiendaController@payment')->name('payment');
	Route::get('orders','tiendaController@orders')->name('orders');
	Route::post('payment','tiendaController@execPayment')->name('execPayment');
	Route::get('downloadVoucher/{id}','tiendaController@downloadVoucher')->name('downloadVoucher');
	Route::get('downloadRecibo/{id}','tiendaController@downloadRecibo')->name('downloadRecibo');

});





Route::resource('/admin/clientes',"clienteController");
Route::get('/admin/payments',"paymentController@index")->name('admin.payments.index');
Route::get('/admin/payments/{id}',"paymentController@show")->name('admin.payments.show');
Route::post('/admin/payments/negar/{id}',"paymentController@negar")->name('admin.payments.negar');

Route::get('/admin/payments/confirmar/{id}',"paymentController@confirmar")->name('admin.payments.confirmar');
Route::delete('/admin/payments/confirmar/{id}',"paymentController@destroy")->name('admin.payments.destroy');

//ClientArea
Route::get('/clientarea',"clientareaController@index")->name('clientarea.index');


//Clientarea proyectos
//Payments
Route::get('/clientarea/payments/create',"clientarea\paymentController@create")->name('clientarea.payment.create');
Route::get('/clientarea/payments/store/{id}',"clientarea\paymentController@store")->name('clientarea.payment.store');
Route::get('/clientarea/payments/{id}',"clientarea\paymentController@show")->name('clientarea.payment.show');
Route::get('/clientarea/payments',"clientarea\paymentController@index")->name('clientarea.payment.index');
Route::get('/clientarea/payments/pdf/{id}',"clientarea\paymentController@facturaPDF")->name('clientarea.payment.facturapdf');





Route::get('/admin',"adminController@index")->name('admin.index');




Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
//Route::get('/home', 'indexController@index')->name('home');
Route::get('/','indexController@index');
Route::middleware('auth')->get('/home',function(){
    
    return Redirect::to('/admin');
});
