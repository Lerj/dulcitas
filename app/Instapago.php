<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instapago extends Model
{
    protected $table = "pago_instapago";
    
    protected $primaryKey='id';

    public $timestamps=false;


    protected $fillable = [
        'referencia',
        'invoice_id',
        'voucher'
    ];

   
    function invoice() {
        $this->belongsTo('App\Payments','id','invoice_id');
    }
    protected $guarded = [];

}
