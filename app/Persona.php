<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona
 extends Model
{
    
    protected $table = "persona";
    
    protected $primaryKey='id';

    public $timestamps=false;


    protected $fillable = [
        'name', 'lastname','cedula','phone','address','birthday','users_id'
        
    ];

    public function user() {
        return $this->belongsTo('App\User','users_id','id');
    }
   
}
