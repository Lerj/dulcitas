<?php

namespace App;

use Gloudemans\Shoppingcart\Contracts\Buyable;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';


    protected  $primaryKey = 'id';


    protected $fillable = ['categories_id','code', 'name_product','descripcion','stock','status_id','sale_price','type','image'];

    public function status() {
        return $this->hasOne(status::class,'id','status_id');
    }
    public function category() {
        return $this->hasOne(Categoria::class,'id','categories_id');
    }

}
