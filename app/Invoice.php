<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';

    protected $fillable = ['code','user_id','iva'];

    public $timestamps = true;


    public function getCodeAttribute() {
     //$this->attributes['code'] =
      return str_pad($this->id, 7, "0", STR_PAD_LEFT);
    }



    public function getSubIvaAttribute() {
        return ($this->iva/100)*$this->subtotal;
    }
    public function getTotalAttribute() {
        return $this->subTotal+$this->subIva;
    }
     public function getSubTotalAttribute() {
        $total=0;
        foreach($this->details as $det) {
            $detTotal = $det->price*$det->quantity;
            $total+=$detTotal;
        }
        return $total;
    }
    public function details() {
        return $this->hasMany(InvoiceDetails::class,'invoice_id','id');
    }
    public function payment() {
        return $this->hasOne(Payments::class,'invoice_id','id');
    }

    public function instapago() {
        return $this->hasOne(Instapago::class,'invoice_id','id');
    }
    public function user() {
        return $this->belongsTo(User::class,'user_id','id');
    }


}
