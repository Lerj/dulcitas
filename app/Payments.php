<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = "payments";
    
    protected $primaryKey='id';

    public $timestamps=true;


    protected $fillable = [
        'fecha_pago',
        'status',
        'total',
        'user_id',
        'invoice_id',
        'razon_negado',
        'voucher'
    ];

   
    function invoice() {
        return $this->belongsTo(Invoice::class,'invoice_id','id');
    }
    function instapago() {
        return $this->hasOne('App\Instapago','payment_id','id');
    }
    function user() {
        return $this->belongsTo('App\User','user_id','id');
    }
    protected $guarded = [];

}
