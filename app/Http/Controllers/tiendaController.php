<?php

namespace App\Http\Controllers;
use App\Categoria;
use App\Products;
use App\Invoice;
use App\InvoiceDetails;
use App\Reservations;
use App\Instapago;
use Illuminate\Http\Request;
use Cart;
use \Instapago\Api;
use PDF;
class tiendaController extends Controller
{

    public function index(Request $request) {

        if (!$request->categoria) {
            $products = Products::where([
                ['name_product','LIKE',"%$request->search%"],
                ['status_id','=',1],
//                ['type','!=',1]
            ])->get();
        }else {

            $products = Products::where([
                    ['name_product','LIKE',"%$request->search%"],
                    ['status_id','=',1],
                    ['categories_id','=',$request->categoria],
//                    ['type','!=',1]
                ]
            )->get();
        }
        
        
    	return view('tienda.index',['products' => $products,'categorias' => Categoria::pluck('categoria','id')]);
    }

    public function execPayment(Request $request) {
        
        $expirationYear = explode("/",$request->expiration);
        $month = $expirationYear[0];
        $year = $expirationYear[1];
        $paymentData = [
          'amount' => \Cart::total(),
          'description' => 'Pago',
          'card_holder' => $request->name,
          'card_holder_id' => $request->cedula,
          'card_number' => str_replace(" ","",$request->card),
          'cvc' => $request->code,
          'expiration' => "$month/20$year",
          'ip' => request()->ip(),
        ];
        try{

          $api = new Api(env('INSTAPAGO_KEY_ID'),env('INSTAPAGO_PUBLIC_KEY_ID'));

          $respuesta = $api->directPayment($paymentData);
          if ($respuesta["code"]==201) {
            $invoice = Invoice::create([
                'code' => 10000,
                'iva' => 16,
                'user_id' => \Auth::user()->id

            ]);
            foreach(Cart::content() as $product) {
                InvoiceDetails::create([
                    'products_id' => $product->options->product["id"],
                    'invoice_id' => $invoice->id,
                    'quantity' => $product->qty,
                    'price' => $product->price
                ]);
            }

            Instapago::create([
                'referencia' => $respuesta["reference"],
                'invoice_id' => $invoice->id,
                'voucher' => $respuesta["voucher"]
            ]);




            Cart::destroy();
            return view('tienda.finish',['voucher' => $respuesta["voucher"],"invoice" => $invoice]);

          }
          // hacer algo con $respuesta
        }catch(\Instapago\Exceptions\InstapagoException $e){

          dd($e->getMessage()); // manejar el error

        }catch(\Instapago\Exceptions\AuthException $e){

          dd($e->getMessage()); // manejar el error

        }catch(\Instapago\Exceptions\BankRejectException $e){

          dd($e->getMessage()); // manejar el error

        }catch(\Instapago\Exceptions\InvalidInputException $e){

          dd($e->getMessage()); // manejar el error

        }catch(\Instapago\Exceptions\TimeoutException $e){

          dd($e->getMessage()); // manejar el error

        }catch(\Instapago\Exceptions\ValidationException $e){

          dd($e->getMessage()); // manejar el error

        }
        dd($paymentData);
        //return view('tienda.payment');
    }
    public function payment() {
        if(count(Cart::content())<1) {
            return redirect('tienda');
        }
        return view('tienda.payment');
    }
    public function cart(Request $request) {
        $product = '';
        if ($request->has('id')){
            $product = Products::findOrFail($request->id);
            if (!$product) { return response(['status' => 'error'],403); }
        }else {

        }


        switch($request->action) {
            case 'add':
                if ($request->options["alquiler"]) {
                    if (!$request->options["address"] || !$request->options["horas"] || !$request->options["start_time"] || !$request->options["end_time"]) {
                        return response(['status' => 'error','msg' => "Ingrese todos los datos"],403);
                    }

                    $horas = $request->options["horas"];
                    $start_time = $request->options["start_time"];
                    $end_time = $request->options["end_time"];
                    $address = $request->options["address"];
                    $fecha = $request->options["fecha"];
                    $sale_price = $request->options["sale_price"];
                    $res = Reservations::where('fecha','=',$request->options["fecha"])->get()->count();
                    if ($res>=$product->stock) {
                        return response(['status' => 'error','msg' => "El producto \"$product->name_product\" no está disponible para la fecha $fecha"],403);

                    }

                    $p = Cart::search(function ($cartItem, $rowId) use ($request) {
                        return $cartItem->id === $request->id;
                    });

                    if (count($p)<1) {
                        Cart::add($request->id,"(Alquiler) ".$product->name_product, $request->quantity, $request->options["total"],['image' => $product->image,'alquiler' => true,'horas' => $horas,'fecha' => $fecha, 'end_time' => $end_time,'start_time' => $start_time,'address' => $address,'sale_price' => $sale_price ]);
                    }else {
                        return response(['status' => 'error','msg' => "El producto \"$product->name_product\" ya está registrado en el carrito"],403);
                    }
                }else {
                    Cart::add($request->id,$product->name_product, $request->quantity, $product->sale_price,['image' => $product->image]);
                }

                return response(['action' => $request->action,'product' => $product,'cart' => Cart::content(),'total' => Cart::subtotal()]);
                break;

            case 'remove':
                Cart::remove($request->code);
                return response(['action' => $request->action,'product' => $product,'cart' => Cart::content(),'total' => Cart::subtotal()]);
                break;
            case 'empty':
                Cart::destroy();
                return response(['action' => $request->action]);
                break;
        }
    }

    public function checkout() {
        //dd(Cart::content());

        return view('tienda.checkout');
    }


    public function downloadRecibo($id) {

        $invoice = Invoice::with('instapago','details','user')->findOrFail($id);
        //$pdf = App::make('dompdf.wrapper');
        //$pdf->loadHTML('<h1>Test</h1>');
        //return $pdf->stream();
        $pdf = PDF::loadView('pdf.factura', compact('invoice'));
        return $pdf->stream('invoice.pdf');
    }

    public function downloadVoucher($id) {

        $invoice = Invoice::with('instapago')->findOrFail($id);
        //$pdf = App::make('dompdf.wrapper');
        //$pdf->loadHTML('<h1>Test</h1>');
        //return $pdf->stream();

        $pdf = PDF::loadHTML($invoice->instapago->voucher);
        return $pdf->stream("voucher-$invoice->code.pdf");
    }

    public function show($id) {
        $product = Products::findOrFail($id);
        return view('tienda.showproduct',['product' => $product]);
    }
  
    public function add(Request $request) {
        $product = Products::findOrFail($request->id);

        parse_str($request->options, $options);

        $options["product"] = $product;
        $c = Cart::add($product->id,$product->name_product,$request->qty,$request->orderPrice,$options);
        Cart::setTax($c->rowId,16);
        return ["success" => true,"c" => $c];
    }
    public function getCart() {
        return Cart::content();
    }
    public function del(Request $request,$rowId) {
        $c = Cart::remove($rowId);
        return ["success" => true,"c" => $c];

    }
    public function products() {
        return Products::all();
    }


    public function orders() {
        $orders = \Auth::user()->invoices;

        $orders->load('details','instapago','details.product');

        
        return view('tienda.orders',compact('orders'));

    }
}
