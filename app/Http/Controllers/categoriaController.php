<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoriaController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|root');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categoria::all();
        return view('admin.category.index',["categories" => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'categoria' => 'required|string|min:2|max:50',

            'descripcion'=> 'required|string|min:5|max:500',

            ]);
        $category = new Categoria();
        $category->categoria =$request->categoria;
        $category->descripcion=$request->descripcion;
        $category->save();
        return \redirect()->to('admin/categorias');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categoria)
    {

        return view('admin.category.edit',["category" => $categoria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categoria  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categoria)
    {
        $this->validate($request,[
            'categoria' => 'required|string|min:2|max:50',

            'descripcion'=> 'required|string|min:5|max:100',

        ]);

        $categoria->categoria = $request->categoria;
        $categoria->descripcion= $request->descripcion;
        $categoria->update();
        return \redirect()->to('admin/categorias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categoria  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categoria)
    {


        if ($categoria->delete()) {
            return \redirect()->to('admin/categorias');
            }else{
            die('Error al borrar');
        }

    }
}
