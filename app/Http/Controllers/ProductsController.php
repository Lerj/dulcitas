<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Products;
use App\status;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|root');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        //$column, $operator = null, $value = null, $boolean = 'and'
        if (!$request->category) {
            $products = Products::where('status_id','!=',3)->get();
        }else {
            /*$products = Products::where(function($query) use ($request) {
                $query->where(
                    ['status_id','!=',3],
                    ['categories_id','==',$request->categoria]);

                

            })->get();*/
            $products = Products::where([
                ['status_id','!=',3],
                ['categories_id','=',$request->category]
            ])->get();
        }
        return view('admin.products.index ',['products'=>$products,'categories'=>Categoria::pluck('categoria','id')]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.products.create',['categories'=>Categoria::pluck('categoria','id'),'status'=>status::pluck('status','id')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'name_product' => 'string|max:50|required',
           'stock' => 'integer|min:1,required',
           'categories_id' => 'required',
           'code' => 'required|max:99|required',
           'status_id' => 'required',
           'detail' => 'string',
           'sale_price' => 'numeric'
        ]);

        //$request->request->add();
        $p = Products::create($request->all());

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();

           
            $name = "product-$p->id".".$extension";
            $destinationPath = public_path('/images/products');
            $image->move($destinationPath, $name);
            $p->image = $name;
            $p->update();

        }
        return redirect('admin/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\products  $products
     * @return \Illuminate\Http\Response
     */
    public function show(products $product)
    {

        return view('store.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(products $product)
    {

        return view('admin.products.edit',['product' => $product,'categories' => Categoria::pluck('categoria','id'),'status' => status::pluck('status','id')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, products $product)
    {
         $this->validate($request,[
           'name_product' => 'string|max:50|required',
           'stock' => 'integer|min:1,required',
           'categories_id' => 'required',
           'code' => 'required|max:99|required',
           'status_id' => 'required',
           'detail' => 'string',
           'sale_price' => 'numeric'
        ]);

        //$request->request->add();
        $product->fill($request->all());
        if ($request->type==1) {
            $product->type=1;
        }else {
            $product->type=0;
        }
        $product->update();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $extension = $image->getClientOriginalExtension();


            $name = "product-$product->id".".$extension";
            $destinationPath = public_path('/images/products');
            $image->move($destinationPath, $name);
            $product->image = $name;
            $product->update();

        }
        return redirect('admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(products $product)
    {
        $product->status_id = 3;
        if ($product->update()) {


            return redirect('admin/products');
        }
    }
}
