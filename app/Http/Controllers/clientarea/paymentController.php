<?php

namespace App\Http\Controllers\clientarea;
use App\Invoice;
use App\InvoiceDetails;
use App\Products;
use User;
use Session;
use DB;
use Auth;
use App\Payments;
use App\Cuenta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Cart;
use App\Http\Controllers\Controller;

class paymentController extends Controller
{

    
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('role:cliente');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        //$pagos = Auth::user()->payments;
        /*$pagos = Auth::user()->whereHas('payments',function($query) {
             $query->where('status','>',0);
        })->get(); 
        */
        $pagos = Payments::with('invoice','user')->where([['status','>',0],['user_id','=',Auth::user()->id]])->get();
       //dd($pagos[0]->invoice->details);
        $status = ['Eliminado','Pendiente por consignar','Pendiente por aceptación','Pagado','Negado'];
        return view('clientarea.payments.index',["pagos"=>$pagos,"status"=>$status]);
    }
    public function show($id) {
        $payment = Payments::findOrFail($id);
        $products = $payment->invoice->details;

        $status = ['Eliminado','Pendiente por consignar','Pendiente por aceptación','Pagado'];
        return view('clientarea.payments.show',["payment" => $payment, "products"=>$products,"status"=>$status]);
    }
    public function create() {
        if (\Cart::count()>0) {
            //dd(Session::get('pago'));
            $lastcode = Invoice::orderBy('id','desc')->limit(1)->first();

            if ($lastcode) {
                $code = $lastcode->code++;
            }else {
                $code = 10000;
            }

            $invoice = new Invoice();

            $invoice->user_id = Auth::user()->id;
            $invoice->code = $code;
            $invoice->iva = Cart::content()->first()->taxRate;

            $invoice->save();
            //Cart::store('username');



            foreach(Cart::content() as $item) {
                $product = Products::findOrFail($item->id);

                $details = InvoiceDetails::create([
                    'invoice_id' => $invoice->id,
                    'products_id' => $product->id,
                    'quantity' => $item->qty,
                    'price' => $item->price,
                    'options' => $item->options


                ]);
            }



            $pago = new Payments();
            $pago->invoice_id = $invoice->id;
            $pago->total = Cart::subtotal();
            $pago->user_id = Auth::user()->id;
            $pago->status = 1;
            $pago->save();
            Cart::destroy();



            Session::flash('alert',["tipo"=>"info","mensaje"=>"Pago solicitado, debe consignar para completar su operación"]);
            return redirect('clientarea/payments');
        }
    }
    public function store(Request $request,$id) {
        $pago = Payments::findOrFail($id);
        $cuenta_paypal = $request->get('cuenta_paypal');
        $referencia_bancaria = $request->get('referencia_bancaria');
            if ($cuenta_paypal) {
                $pago->status = 2;
                $pago->fecha_pago = Carbon::now()->toDatetimeString();
                $paypal = new Paypal();
                $paypal->payment_id = $pago->id;
                $paypal->cuenta = $cuenta_paypal;
                $paypal->save();
                
                $pago->update();
                Session::flash('alert',["tipo"=>"success","mensaje"=>"Pago confirmado, su solicitud debe ser aprobada por un administrador"]);
                return redirect('clientarea/payments');
            }
            if ($referencia_bancaria) {
                $pago->status = 2;
                $pago->fecha_pago = Carbon::now()->toDatetimeString();
                $paypal = new Cuenta();
                $paypal->payment_id = $pago->id;
                $paypal->referencia = $referencia_bancaria;
                $paypal->save();
                
                $pago->update();
                Session::flash('alert',["tipo"=>"success","mensaje"=>"Pago confirmado, su solicitud debe ser aprobada por un administrador"]);
                return redirect('clientarea/payments');
            }
        
        
        
    }

    public function facturaPDF($id) {
        $payment = Payments::findOrFail($id);
        $invoice = $payment->invoice;
        $products = $invoice->details;


        $view =  \View::make('pdf.factura',['invoice' => $invoice,'products'=>$products,'payment' => $payment])->render();


        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);
        return $pdf->stream("Factura-N$id.pdf");

    }
    

}
