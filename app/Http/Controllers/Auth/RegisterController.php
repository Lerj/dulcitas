<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Role;
use App\Persona;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected function registered(Request $request, $user)
    {
        if ($user->hasRole('cliente')) {
            return redirect('/');
        }else{
            return redirect('admin');
        }
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'address' => 'required|string|max:500',
            'user' => 'required|string|max:20|unique:users',
            'phone' => 'required|numeric',
            
            'cedula' => 'required|numeric|max:99999999',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'birthday'=>'date',
        ]);
    }  
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'user' => $data['user'],
            'password' => bcrypt($data['password'])
            
        ]);
           
        $cliente = Persona::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'phone'=> $data['phone'],
            'address' => $data['address'],
            'birthday'=> \Carbon\Carbon::parse($data['birthday'])->toDateString(),
            'cedula' => $data['cedula'],
            'users_id'=>$user->id
        ]);
        $user->attachRole(Role::find(3));
        return $user;
    }

}
