<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model
{
    protected $table = 'invoice_details';

    protected $fillable = ['invoice_id','products_id','quantity','price','options'];
    protected $casts = [
        'options' => 'array'
    ];
    public $timestamps = false;

    public function invoice() {
        return $this->belongsTo(Invoice::class,'id','invoice_id');

    }
    public function product() {
        return $this->belongsTo(Products::class,'products_id','id');
    }

}
